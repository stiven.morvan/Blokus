package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import controler.ListenerCards;

/**
 * The Class MenuLan is the menu to do a game online.
 */
public class MenuLan extends JPanel{
	
	/**
	 * Instantiates a new menuLan.
	 *
	 * @param blokus the blokus frame
	 */
	public MenuLan(Blokus blokus){
		this.setLayout(new BorderLayout());
		
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel main = new JPanel();
	    main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JLabel sousTitre = new JLabel("Creation d'une partie");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 50));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        JPanel button = new JPanel();
        
        Border borderButton = new LineBorder(gris, 30);
		
		JButton jouer = new JButton(new ImageIcon("data/icons/user.png"));
		jouer.setBackground(gris);
		jouer.setPreferredSize(new Dimension(260, 260));
		jouer.setBorder(borderButton);
		ListenerCards lPlay = new ListenerCards(blokus, "Connexion","user");
		jouer.addActionListener(lPlay);
		jouer.addMouseListener(lPlay);
		button.add(jouer);
		
		JButton lan = new JButton(new ImageIcon("data/icons/serveur.png"));
		lan.setBackground(gris);
		lan.setPreferredSize(new Dimension(260, 260));
		lan.setBorder(borderButton);
		ListenerCards lLan = new ListenerCards(blokus, "Serveur", "serveur");
		lan.addActionListener(lLan);
		lan.addMouseListener(lLan);
		button.add(lan);
		
		JButton back  = new JButton("Retour");
        back.setBackground(new Color(165, 110, 45));
        back.setMaximumSize(new Dimension(160, 35));
        back.setForeground(Color.WHITE);
        back.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
        back.setOpaque(true);
        back.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        back.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerCards lCard = new ListenerCards(blokus, "Main");
        back.addActionListener(lCard);
		
		button.setBorder(new LineBorder(gris, 5));
		button.setBackground(gris);
		
		main.setBorder(new LineBorder(gris, 10));
		main.setBackground(gris);
		
		main.add(sousTitre);
		main.add(button);
		
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
		this.add(back, BorderLayout.SOUTH);
	}
}

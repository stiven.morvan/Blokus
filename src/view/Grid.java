package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controler.GridListener;
import model.Map;
import model.Piece;
import model.Position;

public class Grid extends JPanel{
	
	/** If the advanced cursor is activate. */
	private boolean cursor;
	
	/** All jLabel of the grid. */
	private JLabel[][] jLabelTab;
	
	/**
	 * The last position of the curso in the grid.
	 */
	private Position lastTemporalPosition;
	
	/**
	 * Instantiates a new grid.
	 */
	public Grid(){
	    this.setLayout(new GridLayout(20,20));
	    this.setSize(500, 500);
	    this.setMinimumSize(new Dimension(500, 500));
		this.setPreferredSize(new Dimension(500, 500));
	    jLabelTab = new JLabel[20][20];
	    
	    Color gris = new Color(59, 59, 59);
	    LineBorder border = new LineBorder(gris, 1);
	    
	    for (int x = 0; x < 20; x++) {
	    	for (int y = 0; y < 20; y++) {
				JLabel label = new JLabel("");
				jLabelTab[x][y] = label;
				label.setBackground(new Color(20, 20, 20));
				label.setSize(new Dimension(2, 2));
				label.setMinimumSize(new Dimension(2, 2));
				label.setPreferredSize(new Dimension(2, 2));
				label.setOpaque(true);
				label.setBorder(border);
				
				this.add(label);
			}
		}
	    
	    this.setVisible(true);
	    
	    this.lastTemporalPosition = null;
	}
	
	/**
	 * Sets if the advanced cursor is activate or no.
	 *
	 * @param b the state of the advanced cursor
	 */
	public void setCursor(boolean b){
		cursor = b;
	}
	
	/**
	 * Adds the piece in the grid a the position given.
	 * (change the color of the jLabel corresponding)
	 *
	 * @param piece the new piece
	 * @param position the position of the piece
	 */
	public void addPiece(Piece piece, Position position){
		Color couleur = piece.getPlayer().getColor();
		
		int pX = position.getX();
		int pY = position.getY();
		
		for (int x = -2; x < 3; x++) {
	    	for (int y = -2; y < 3; y++) {
				if(piece.getPattern()[y+3][x+3] == 1){
					jLabelTab[y+pY][x+pX].setBackground(couleur);
				}
			}
		}
	}
	
	/**
	 * Adds the fragment of piece. (Change the color of only one jLabel at the position)
	 *
	 * @param piece the piece
	 * @param position the position
	 */
	public void addFragmentOfPiece(Piece piece, Position position){
		Color couleur = piece.getPlayer().getColor();
		
		int pX = position.getX();
		int pY = position.getY();
		
		jLabelTab[pY][pX].setBackground(couleur);
	}

	/**
	 * Adds the future piece temporally. (change the border where the piece can be place)
	 *
	 * @param piece the futur piece
	 * @param position the actual position
	 */
	public void addTemporalPiece(Piece piece, Position position){
		if (!cursor) {
			Color couleur = piece.getPlayer().getColor();
			this.lastTemporalPosition = position;
			
			int pX = position.getX();
			int pY = position.getY();
			
			for (int y = -2; y < 3; y++) {
		    	for (int x = -2; x < 3; x++) {
					if(piece.getPattern()[y+3][x+3] == 1){
						if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
							jLabelTab[y+pY][x+pX].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, couleur));
						}
					}
				}
			}
		}
		else{
			addTemporalPieceDev(piece, position);
		}
	}
	

	/**
	 * Adds the future developer piece temporally. (change the border where the piece can be place)
	 * And add the pattern of the spot where the box must be empty or with the same player piece.
	 *
	 * @param piece the futur piece
	 * @param position the position
	 */
	public void addTemporalPieceDev(Piece piece, Position position){
		Color couleur = piece.getPlayer().getColor();
		this.lastTemporalPosition = position;
		
		int pX = position.getX();
		int pY = position.getY();
		
		for (int y = -3; y < 4; y++) {
	    	for (int x = -3; x < 4; x++) {
	    		if(piece.getPattern()[y+3][x+3] == 1){
					if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
						jLabelTab[y+pY][x+pX].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, couleur));
					}
				}
	    		if(piece.getPattern()[y+3][x+3] == 3){
					if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
						jLabelTab[y+pY][x+pX].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
					}
				}
	    		if(piece.getPattern()[y+3][x+3] == 2){
					if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
						jLabelTab[y+pY][x+pX].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.ORANGE));
					}
				}
			}
		}
	}

	/**
	 * Delete last temporal piece.
	 */
	public void deleteLastTemporalPiece(){
		if(!cursor){
			if(lastTemporalPosition != null){
				int pX = lastTemporalPosition.getX();
				int pY = lastTemporalPosition.getY();
				
				for (int y = -2; y < 3; y++) {
			    	for (int x = -2; x < 3; x++) {
						if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
							jLabelTab[y+pY][x+pX].setBorder(new LineBorder(new Color(59, 59, 59), 1));;
						}
					}
				}
			}
		} else {
			deleteLastTemporalPieceDev();
		}
	}
	
	/**
	 * Delete last developer temporal piece.
	 */
	public void deleteLastTemporalPieceDev(){
		if(lastTemporalPosition != null){
			int pX = lastTemporalPosition.getX();
			int pY = lastTemporalPosition.getY();
			
			for (int y = -3; y < 4; y++) {
		    	for (int x = -3; x < 4; x++) {
					if((y+pY) >= 0 && (x+pX) >= 0 && (y+pY) < jLabelTab.length && (x+pX) < jLabelTab[y+pY].length ){
						jLabelTab[y+pY][x+pX].setBorder(new LineBorder(new Color(59, 59, 59), 1));;
					}
				}
			}
		}
	}
	
	/**
	 * Gets the jLabelTab board.
	 *
	 * @return the jLabelTab
	 */
	public JLabel[][] getjLabelTab() {
		return jLabelTab;
	}
	
	/**
	 * Sets the grid listener on all jLabel of this grid.
	 *
	 * @param gl the new gridListener
	 */
	public void setGridListener(GridListener gl){
		for (JLabel[] jLabels : jLabelTab) {
			for (JLabel jLabel : jLabels) {
				jLabel.addMouseListener(gl);
				jLabel.addMouseWheelListener(gl);
			}
		}
	}
}

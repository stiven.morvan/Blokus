package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controler.Game;
import controler.MainButtonListener;
import model.Player;

/**
 * The Class ZoneInformation is a jpanel place in the bottom of a viewGame.
 * In this panel the score are display and the button to quit or save are display.
 */
public class ZoneInformation extends JPanel{
	
	/** The left panel. He contain the score when the game is finish. */
	private JPanel gauche;
	
	/** The button who allow to quit or save the game.*/
	private JButton sauvegarder, quitter;
	
	/** The game. */
	private Game game;
	
	/**
	 * Instantiates a new zone information.
	 *
	 * @param blokus the blokus
	 * @param game the game
	 */
	public ZoneInformation(Blokus blokus, Game game){
		Color gris = new Color(77, 77, 77);
		Color grisF = new Color(45, 45, 45);
		
		this.setLayout(new GridLayout(1,3));
		this.game = game;
		JPanel milieu = new JPanel();
		
		gauche = new JPanel();
		gauche.setBackground(grisF);
		
		JLabel milieuBlokus = new JLabel("Blokus");
		milieuBlokus.setFont(new Font("Arial", Font.PLAIN, 25));
		milieuBlokus.setForeground(new Color(220, 220, 220));
		milieu.add(milieuBlokus);
		milieu.setBackground(grisF);
	    this.setBackground(grisF);
	    
	    JPanel mesBoutons = new JPanel();
	    mesBoutons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		quitter = new JButton("Quitter");
		quitter.setBackground(gris);
		quitter.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(100, 100, 100)));
		quitter.setPreferredSize(new Dimension(90,30));
		quitter.setForeground(new Color(220, 220, 220));
		sauvegarder = new JButton("Sauvegarder");
		sauvegarder.setBackground(gris);
		sauvegarder.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(100, 100, 100)));
		sauvegarder.setPreferredSize(new Dimension(90,30));
		sauvegarder.setForeground(new Color(220, 220, 220));
		mesBoutons.add(sauvegarder);
		mesBoutons.add(quitter);
		mesBoutons.setBackground(grisF);
		this.add(mesBoutons, BorderLayout.SOUTH);
		this.setBorder(BorderFactory.createMatteBorder(7, 7, 7, 7, grisF));
		MainButtonListener listener = new MainButtonListener(blokus, game);
	    sauvegarder.addActionListener(listener);
	    quitter.addActionListener(listener);
		
		this.add(gauche);
		this.add(milieu);
		this.add(mesBoutons);
	}
	
	/**
	 * Get the save button.
	 *
	 * @return the sauvegarder
	 */
	public JButton getSauvegarder() {
		return sauvegarder;
	}
	
	/**
	 * Gets the quit button.
	 *
	 * @return the quitter
	 */
	public JButton getQuitter() {
		return quitter;
	}
	
	/**
	 * Set the game.
	 *
	 * @param g the new game
	 */
	public void setGame(Game g){
		game = g;
	}
	
	/**
	 * Print the score in gauche.
	 */
	public void afficherScore(){
		gauche.removeAll();
		gauche.setLayout(new BoxLayout(gauche, BoxLayout.PAGE_AXIS));
		JLabel score = new JLabel("Scores : ");
		score.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		score.setFont(new Font("Arial", Font.PLAIN, 15));
		score.setForeground(new Color(220, 220, 220));
		gauche.add(score);
		
		JLabel scoreJoueur;
		Player[] joueurs = game.getGameRule().getScoreTab();
		for (int i = 0; i < joueurs.length; i++) {
			scoreJoueur = new JLabel((i+1)+" : "+joueurs[i].getName()+" / "+joueurs[i].getScore()+" Points");
			scoreJoueur.setFont(new Font("Arial", Font.PLAIN, 10));
			scoreJoueur.setForeground(new Color(220, 220, 220));
			gauche.add(scoreJoueur);
		}
		
		this.repaint();
		this.revalidate();
	}
}

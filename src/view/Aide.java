package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controler.ListenerCards;

/**
 * The Class Aide is a jpanel where the information of the functionnment of this game are explain..
 */
public class Aide extends JPanel{
	
	/**
	 * Instantiates a new aide panel.
	 *
	 * @param blokus the blokus frame
	 */
	public Aide(Blokus blokus){
		this.setLayout(new BorderLayout());
		
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel main = new JPanel();
	    main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JLabel sousTitre = new JLabel("Commandes");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 30));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 24));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        JLabel help = new JLabel(new ImageIcon("data/tuto.png"));
        help.setOpaque(true);
        help.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        help.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        JButton back  = new JButton("Retour");
        back.setBackground(new Color(165, 110, 45));
        back.setMaximumSize(new Dimension(160, 35));
        back.setForeground(Color.WHITE);
        back.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
        back.setOpaque(true);
        back.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        back.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerCards lCard = new ListenerCards(blokus, "Main");
        back.addActionListener(lCard);

        main.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		main.setBackground(gris);
		main.setBorder(BorderFactory.createEmptyBorder(0,0,180,0));
		main.setMaximumSize(new Dimension(100, 100));
		main.setPreferredSize(new Dimension(100, 100));
		main.setSize(new Dimension(100, 100));
				
        
		main.add(sousTitre);
		main.add(help);
        
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
		this.add(back, BorderLayout.SOUTH);
	}
}

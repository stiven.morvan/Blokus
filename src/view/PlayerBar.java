package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Player;

/**
 * This jPanel display the statue of his player : his name, score and number of piece.
 */
public class PlayerBar extends JPanel{
	
	/** The jPanel of the score of the player. */
	private JLabel score;
	
	/** The jPanel of the number of pieces of the player. */
	private JLabel pieces;
	
	/** The jPanel of the name of the player. */
	private JLabel name;
	
	/** The player. */
	private Player player;
	
	/**
	 * Instantiates a new player bar.
	 *
	 * @param player the player
	 */
	public PlayerBar(Player player){
		this.player = player;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		name = new JLabel(player.getName(), JLabel.CENTER);
		name.setFont(new Font("Arial", Font.PLAIN, 20));
		name.setForeground(new Color(220, 220, 220));
		name.setAlignmentX(CENTER_ALIGNMENT);
		JPanel panel = new JPanel();
		panel.setBackground(new Color(59, 59, 59));
		panel.setLayout(new GridLayout(0,2));
		score = new JLabel(""+player.getScore());
		score.setForeground(new Color(220, 220, 220));
		pieces = new JLabel(player.getActualNbPiece() + " / " + player.getPieces().length);
		pieces.setForeground(new Color(220, 220, 220));
		
		panel.add(score);
		panel.add(pieces);
		
		this.add(name);
		this.add(panel);
	    this.setBackground(new Color(59, 59, 59));
	}
	
	/**
	 * Change the name color by the color of the player
	 */
	public void isPlaying(){
		name.setForeground(player.getColor());
	}
	
	/**
	 * Change the name color by the standart grey color
	 */
	public void isWaiting(){
		name.setForeground(new Color(220, 220, 220));
	}
	
	/**
	 * Change the name color by the a dark grey color
	 */
	public void isFinish(){
		name.setForeground(new Color(100, 100, 100));
	}
	
	/**
	 * Set the player.
	 *
	 * @param player the new player
	 */
	public void setPlayer(Player player){
		this.player = player;
	}
	
	/**
	 * Actualize the score and the number of pieces.
	 */
	public void actualiser(){
		this.score.setText(""+player.getScore());
		this.pieces.setText(player.getActualNbPiece() + " / " + player.getPieces().length);
		this.score.repaint();
		this.pieces.repaint();
	}
}

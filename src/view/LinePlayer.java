package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import controler.ListenerPlayersMenu;

/**
 * The Class LinePlayer is a class who build a JPanel used to define a player (its name, color and if it is a IA).
 */
public class LinePlayer {
	
	/** The color of the player. */
	private JButton couleur;
	
	/** If he is a IA. */
	private JButton ia;
	
	/** Hes name. */
	private JTextField nom;
	
	/** The line who define the player. */
	private JPanel line;
	
	/** The dropdown list where we can select the level of the IA. */
	private JComboBox<String> listDeroulante;
	
	/**
	 * Instantiates a new linePlayer.
	 *
	 * @param name the name of the player
	 * @param color the color of the player
	 * @param listenerCouleur the listenerCouleur of this line
	 * @param listenerIA the listenerIA of this line
	 * @param online If the game is the online
	 */
	public LinePlayer(String name, Color color, ListenerPlayersMenu listenerCouleur, ListenerPlayersMenu listenerIA, boolean online){
		Color gris = new Color(77, 77, 77);
		Color grisF = new Color(59, 59, 59);
		line = new JPanel();
	    line.setLayout(new FlowLayout());
	    
	    nom = new JTextField(10);
	    nom.setPreferredSize(new Dimension(80, 30));
	    nom.setFont(new Font("Gotham Medium", Font.PLAIN, 16));
	    nom.setForeground(new Color(240, 240, 240));
	    nom.setBorder(new LineBorder(grisF, 5, false));
	    nom.setText(name);
	    
	    JLabel couleurText = new JLabel("      Couleur:");
	    couleurText.setBackground(gris);
	    couleurText.setFont(new Font("Gotham Medium", Font.PLAIN, 18));
	    couleurText.setForeground(new Color(240, 240, 240));
	    couleurText.setPreferredSize(new Dimension(110, 30));
	    couleurText.setOpaque(true);
	    
	    couleur = new JButton();
	    couleur.setBackground(color);
	    couleur.setPreferredSize(new Dimension(28, 28));
	    couleur.setOpaque(true);
	    couleur.setBorder(new LineBorder(grisF, 4, false));
	    couleur.addActionListener(listenerCouleur);
	    JLabel aiText = null;
	    if(!online){
	    	aiText = new JLabel("      IA:");
		    aiText.setBackground(gris);
		    aiText.setFont(new Font("Gotham Medium", Font.PLAIN, 18));
		    aiText.setForeground(new Color(240, 240, 240));
		    aiText.setPreferredSize(new Dimension(65, 30));
		    aiText.setOpaque(true);
		    
		    String[] levelTab = new String[]{"DEBUTANT", "INTERMEDIAIRE", "DIFFICILE"};
			listDeroulante = new JComboBox<>(levelTab);
			listDeroulante.setPreferredSize(new Dimension(120, 4+listDeroulante.getPreferredSize().height));
			listDeroulante.setVisible(false);
			listDeroulante.setBackground(gris);
			listDeroulante.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
			listDeroulante.setForeground(new Color(240, 240, 240));
			listDeroulante.setBorder( new LineBorder(grisF, 3));
			for (int i = 0; i < listDeroulante.getComponentCount(); i++) {
				if (listDeroulante.getComponent(i) instanceof JComponent) {
			        ((JComponent) listDeroulante.getComponent(i)).setBorder(new LineBorder(grisF, 1));
		    	}
			}
		    
		    ia = new JButton();
		    ia.setBackground(new Color(35, 35, 35));
		    ia.setPreferredSize(new Dimension(28, 28));
		    ia.setOpaque(true);
		    ia.setBorder(new LineBorder(grisF, 4, false));
		    ia.addActionListener(listenerIA);
	    }
	    nom.setBackground(new Color(59, 59, 59));
	    
	    line.setBackground(gris);
	    
	    line.add(nom);
	    line.add(couleurText);
	    line.add(couleur);
	    if(!online){
	    	line.add(aiText);
		    line.add(ia);
		    line.add(listDeroulante);
	    }
	    line.setMaximumSize(new Dimension(700, 85));
	}

	/**
	 * Gets the color of the player.
	 *
	 * @return the color
	 */
	public JButton getCouleur() {
		return couleur;
	}

	/**
	 * Set the color of the player.
	 *
	 * @param couleur the new color
	 */
	public void setCouleur(JButton couleur) {
		this.couleur = couleur;
	}

	/**
	 * Gets the ia of the player.
	 *
	 * @return the ia
	 */
	public boolean getIa() {
		Color check = new Color(165, 110, 45);
		boolean ret = false;
		if (ia != null && ia.getBackground().equals(check)) {
			ret = true;
		}
		return ret;
	}

	/**
	 * Sets the ia of the player.
	 *
	 * @param ia the new ia
	 */
	public void setIa(JButton ia) {
		this.ia = ia;
	}

	/**
	 * Gets the name of the player.
	 *
	 * @return the name
	 */
	public JTextField getNom() {
		return nom;
	}

	/**
	 * Sets the name.
	 *
	 * @param nom the new name
	 */
	public void setNom(JTextField nom) {
		this.nom = nom;
	}

	/**
	 * Gets the line.
	 *
	 * @return the line
	 */
	public JPanel getLine() {
		return line;
	}

	/**
	 * Set the line.
	 *
	 * @param line the new line
	 */
	public void setLine(JPanel line) {
		this.line = line;
	}
	
	/**
	 * Set the level visible.
	 *
	 * @param b the new level visible
	 */
	public void setLevelVisible(boolean b) {
		this.listDeroulante.setVisible(b);
	}
	
	/**
	 * Gets the level of the ia.
	 *
	 * @return the level
	 */
	public int getLevel() {
		int ret = 0;
		if (listDeroulante.getSelectedItem().equals("DEBUTANT")) {
			ret = 1;
		}
		else if (listDeroulante.getSelectedItem().equals("INTERMEDIAIRE")) {
			ret = 2;
		}
		else if (listDeroulante.getSelectedItem().equals("DIFFICILE")) {
			ret = 3;
		}
		return ret;
	}
}

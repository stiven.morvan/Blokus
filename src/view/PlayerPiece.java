package view;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controler.Drag;
import controler.PieceListener;
import model.IA;
import model.Piece;

/**
 * The Class PlayerPiece is a jPanel who represent a piece with a board of jLabel.
 * This class implements Transferable to the function of drag and drop.
 */
public class PlayerPiece extends JPanel implements Transferable{
	
	/** The piece. */
	private Piece piece;
	
	/** The piece listener. */
	private PieceListener pieceListener;
	
	/** The player zone who contains this piece. */
	private PlayerZone playerZone;
	
	/**
	 * Instantiates a new player piece.
	 *
	 * @param piece the piece to represent
	 * @param playerZone the player zone
	 */
	public PlayerPiece(Piece piece, PlayerZone playerZone){
		this.playerZone = playerZone;
		Color grisF = new Color(50, 50, 50);
		Color gris = new Color(77, 77, 77);
		this.piece = piece;
		boolean[][] pattern = piece.getMinimumPattern();
		this.setLayout( new GridLayout(pattern.length, pattern[0].length));
		JLabel label = new JLabel();
		
		// Cree chaque label en fonction du pattern minimum.
		for (int y = 0; y < pattern.length; y++) {
			for (int x = 0; x < pattern[y].length; x++) {
				label = new JLabel();
				label.setPreferredSize(new Dimension(15,15));
				label.setOpaque(true);
				if(pattern[y][x]){
					label.setBackground(piece.getPlayer().getColor());
					label.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, grisF));
				} else {
				    label.setBackground(gris);
				}
				this.add(label);
			}
		}

		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, gris));
		// Ajout des methodes de drag & drop et listener
		if(!(piece.getPlayer() instanceof IA)){
			this.setTransferHandler(new Drag());
			pieceListener = new PieceListener(playerZone.getBlokus(), playerZone.getGame());
		    this.addMouseListener(pieceListener);
		    this.addMouseMotionListener(pieceListener);
		}
	}
	
	/**
	 * Instantiates a new player piece.
	 *
	 * @param piece the piece to represent
	 */
	public PlayerPiece(Piece piece){
		Color grisF = new Color(50, 50, 50);
		Color gris = new Color(77, 77, 77);
		this.piece = piece;
		boolean[][] pattern = piece.getMinimumPattern();
		this.setLayout( new GridLayout(pattern.length, pattern[0].length));
		JLabel label = new JLabel();
		
		// Cree chaque label en fonction du pattern minimum.
		for (int y = 0; y < pattern.length; y++) {
			for (int x = 0; x < pattern[y].length; x++) {
				label = new JLabel();
				label.setPreferredSize(new Dimension(15,15));
				label.setOpaque(true);
				if(pattern[y][x]){
					label.setBackground(new Color(165, 110, 45));
					label.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, grisF));
				} else {
				    label.setBackground(gris);
				}
				this.add(label);
			}
		}

		this.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, new Color(54, 54, 54)));
	}
	
	/**
	 * Use this piece, set the color gray to it and stop it's listener.
	 */
	public void use(){
		this.removeAll();
		if(!(piece.getPlayer() instanceof IA)) pieceListener.stop();
		
		Color grisF = new Color(50, 50, 50);
		Color gris = new Color(77, 77, 77);
		boolean[][] pattern = piece.getMinimumPattern();
		this.setLayout( new GridLayout(pattern.length, pattern[0].length));
		JLabel label = new JLabel();
		
		for (int y = 0; y < pattern.length; y++) {
			for (int x = 0; x < pattern[y].length; x++) {
				label = new JLabel();
				label.setPreferredSize(new Dimension(15,15));
				label.setOpaque(true);
				if(pattern[y][x]){
					label.setBackground(gris);
					label.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, grisF));
				} else {
				    label.setBackground(gris);
				}
				this.add(label);
			}
		}

		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, gris));
		this.revalidate();
	}
	
	/**
	 * Reuse this piece, recolor it with the players color and start it's listener.
	 */
	public void reUse(){
		this.removeAll();
		if(!(piece.getPlayer() instanceof IA)) pieceListener.start();
		
		Color grisF = new Color(50, 50, 50);
		Color gris = new Color(77, 77, 77);
		boolean[][] pattern = piece.getMinimumPattern();
		this.setLayout( new GridLayout(pattern.length, pattern[0].length));
		JLabel label = new JLabel();
		
		for (int y = 0; y < pattern.length; y++) {
			for (int x = 0; x < pattern[y].length; x++) {
				label = new JLabel();
				label.setPreferredSize(new Dimension(15,15));
				label.setOpaque(true);
				if(pattern[y][x]){
					label.setBackground(piece.getPlayer().getColor());
					label.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, grisF));
				} else {
				    label.setBackground(gris);
				}
				this.add(label);
			}
		}

		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, gris));
		this.revalidate();
	}
	
	/**
	 * If this piece is not used, set the color with a bright gray and stop it's listener.
	 */
	public void finish(){
		if(!piece.getIsUse()){
			this.removeAll();
			if(!(piece.getPlayer() instanceof IA)) pieceListener.stop();
			
			Color grisF = new Color(50, 50, 50);
			Color gris = new Color(77, 77, 77);
			boolean[][] pattern = piece.getMinimumPattern();
			this.setLayout( new GridLayout(pattern.length, pattern[0].length));
			JLabel label = new JLabel();
			
			for (int y = 0; y < pattern.length; y++) {
				for (int x = 0; x < pattern[y].length; x++) {
					label = new JLabel();
					label.setPreferredSize(new Dimension(15,15));
					label.setOpaque(true);
					if(pattern[y][x]){
						label.setBackground(new Color(100, 100, 100));
						label.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, grisF));
					} else {
					    label.setBackground(gris);
					}
					this.add(label);
				}
			}
	
			this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, gris));
			this.revalidate();
		}
	}
	
	/**
	 * Sets the piece.
	 *
	 * @param p the new piece
	 */
	public void setPiece(Piece p){
		this.piece = p;
	}
	
	/**
	 * Gets the piece.
	 *
	 * @return the piece
	 */
	public Piece getPiece(){
		return this.piece;
	}
	
	/**
	 * This method arise from transferable.
	 * It return this PlayerPiece to transfert it. (for the drag and drop)
	 */
	public Object getTransferData(DataFlavor flavor) {
	    return this;
	}
	
	/**
	 * This method arise from transferable and it's return is not important.
	 */
	public DataFlavor[] getTransferDataFlavors() {
		return null;
	}
	
	/**
	 * This method arise from transferable and it's return is not important.
	 */
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return false;
	}
	
	/**
	 * Actualize the PlayerBar of this player.
	 */
	public void actualiserBar(){
		playerZone.getBar().actualiser();
	}
	
	/**
	 * Start the listener of this PlayerPiece.
	 */
	public void activer(){
		pieceListener.start();
	}
	
	/**
	 * Stop the listener of this PlayerPiece.
	 */
	public void desActiver(){
		pieceListener.stop();
	}
}

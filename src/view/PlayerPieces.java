package view;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;

import controler.ListenerParametres;
import model.Piece;
import model.Player;

/**
 * The Class PlayerPieces is a jPanel who store and display all playerPiece of a player.
 */
public class PlayerPieces extends JPanel{
	
	/** The list of playerPiece. */
	private ArrayList<PlayerPiece> playerPieces;
	
	/**
	 * Instantiates a new playerPieces.
	 *
	 * @param p the player
	 * @param playerZone the player zone
	 */
	public PlayerPieces(Player p, PlayerZone playerZone){
		playerPieces = new ArrayList<PlayerPiece>();
		PlayerPiece playerPiece;
		for(Piece piece : p.getPieces()){
			playerPiece = new PlayerPiece(piece, playerZone);
	    	this.add(playerPiece);
	    	playerPieces.add(playerPiece);
	    }
		Dimension d = this.getPreferredSize();
	    this.setPreferredSize(new Dimension(300,(d.width / 300) * 150));
	    this.setBackground(new Color(77, 77, 77));
	}
	
	/**
	 * Instantiates a new playerPieces.
	 *
	 * @param playerZone the player zone
	 * @param array the array
	 */
	public PlayerPieces(PlayerZone playerZone, ArrayList<Piece> array){
		playerPieces = new ArrayList<PlayerPiece>();
		PlayerPiece playerPiece;
		for(Piece piece : array){
			playerPiece = new PlayerPiece(piece);
	    	this.add(playerPiece);
	    	playerPieces.add(playerPiece);
	    }
		Dimension d = this.getPreferredSize();
	    this.setPreferredSize(new Dimension(300,(d.width / 300)* 100));
	    this.setBackground(new Color(77, 77, 77));
	}
	
	/**
	 * Instantiates a new playerPieces.
	 *
	 * @param array the array of piece
	 */
	public PlayerPieces(ArrayList<Piece> array){
		this(null, array);
	}
	
	/**
	 * Instantiates a new playerPieces.
	 *
	 * @param array the array
	 * @param parametres the parametres
	 */
	public PlayerPieces(ArrayList<Piece> array, Parametres parametres){
		playerPieces = new ArrayList<PlayerPiece>();
		PlayerPiece playerPiece;
		int i = 0;
		for(Piece piece : array){
			playerPiece = new PlayerPiece(piece);
	    	this.add(playerPiece);
	    	playerPieces.add(playerPiece);
	    	ListenerParametres lp = new ListenerParametres(parametres, i, playerPiece);
	    	playerPiece.addMouseListener(lp);
	    	i++;
	    }
		Dimension d = this.getPreferredSize();
	    this.setPreferredSize(new Dimension(300,(d.width / 300)* 100));
	    this.setBackground(new Color(77, 77, 77));
	}
	
	/**
	 * Refresh the board of PlayerPiece.
	 *
	 * @param array the board
	 */
	public void refresh(ArrayList<Piece> array, Parametres parametres){
		this.removeAll();
		playerPieces = new ArrayList<PlayerPiece>();
		PlayerPiece playerPiece;
		int i = 0;
		for(Piece piece : array){
			playerPiece = new PlayerPiece(piece);
	    	this.add(playerPiece);
	    	playerPieces.add(playerPiece);
	    	if (parametres != null) {
	    		ListenerParametres lp = new ListenerParametres(parametres, i, playerPiece);
		    	playerPiece.addMouseListener(lp);
		    	i++;
			}
	    }
		this.invalidate();
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Use finish on all of PlayerPiece.
	 */
	public void finishPieces(){
		for(PlayerPiece p : playerPieces){
			p.finish();
		}
	}
	
	/**
	 * Use the methode use on all of PlayerPiece used.
	 */
	public void synchro(){
		for(PlayerPiece p : playerPieces){
			if(p.getPiece().getIsUse()){
				p.use();
			}
		}
	}
	
	/**
	 * Set the player in paramters on all of PlayerPiece and use the methode use on all of PlayerPiece used.
	 *
	 * @param p the new player
	 */
	public void synchro(Player p){
		Iterator<PlayerPiece> i = playerPieces.iterator();
		for(Piece piece : p.getPieces()){
			i.next().setPiece(piece);
	    }
		this.synchro();
	}

	/**
	 * Activate all PlayerPiece who are not used.
	 */
	public void activer(){
		for (PlayerPiece playerPiece : playerPieces) {
			if(!playerPiece.getPiece().getIsUse()) playerPiece.activer();
		}
	}
	
	/**
	 * Deactivate all PlayerPiece who are not used.
	 */
	public void desActiver(){
		for (PlayerPiece playerPiece : playerPieces) {
			playerPiece.desActiver();
		}
	}
}














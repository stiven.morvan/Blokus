package view;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * This glassPane is used to animate the drag and drop of the pieces in the grid, with transparency.
 */
public class GlassPane extends JPanel {

	/** The image of the piece. */
	private BufferedImage img;
	
	/** The location. */
	private Point location;
	
	/** The transparency effect on this JPanel. */
	private Composite transparence;

	/**
	 * Instantiates a new glassPane with a transparency of 70%.
	 */
	public GlassPane() {
		setOpaque(false);
		transparence = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.70f);
	}
	
	/**
	 * Set the location of the actual piece.
	 */
	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * Sets the image of the actual piece.
	 *
	 * @param image the new image of the piece
	 */
	public void setImage(BufferedImage image) {
		img = image;
	}
	
	/**
	 * Display the transparency piece in the frame.
	 */
	public void paintComponent(Graphics g) {
		if (img != null) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setComposite(transparence);
			g2d.drawImage(img, (int)(location.getX()-(img.getWidth(this) / 2)), (int)(location.getY()-(img.getHeight(this) / 2)), null);
		}
	}
}

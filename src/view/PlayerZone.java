package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controler.Game;
import controler.PlayerButtonListener;
import model.IA;
import model.Player;

/**
 * The Class PlayerZone is a jPanel who display the playerBar and playerPieces of a player.
 */
public class PlayerZone extends JPanel{
	
	/** The player bar. */
	private PlayerBar playerBar;
	
	/** The player. */
	private Player player;
	
	/** The scrollPane who contains the playerPieces. */
	private JScrollPane scrollPane;
	
	/** The button abandonner and passer. */
	private JButton abandonner, passer;
	
	/** The playerPieces. */
	private PlayerPieces playerPieces;
	
	/** The blokus. */
	private Blokus blokus;
	
	/** The game. */
	private Game game;
	
	/**
	 * Instantiates a new player zone.
	 *
	 * @param p the player
	 * @param game the game
	 * @param blokus the blokus
	 */
	public PlayerZone(Player p, Game game, Blokus blokus){
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		this.setLayout(new BorderLayout());
		this.setBackground(grisF);
		this.player = p;
		this.game = game;
		this.blokus = blokus;
		
		JPanel bar = new JPanel();
		playerBar = new PlayerBar(p);
		bar.add(playerBar);
		bar.setBackground(grisF);
		this.add(bar, BorderLayout.NORTH);
		
		playerPieces = new PlayerPieces(p, this);
		scrollPane = new JScrollPane(playerPieces);
		scrollPane.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, grisF));
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
		scrollPane.getVerticalScrollBar().setBackground(gris);
		scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension( scrollPane.getVerticalScrollBar().getPreferredSize().width-7,  scrollPane.getVerticalScrollBar().getPreferredSize().height));
		this.add(scrollPane, BorderLayout.CENTER);
		
		if(!(player instanceof IA)){
			PlayerButtonListener listener = new PlayerButtonListener(p, this, game);
			JPanel mesBoutons = new JPanel();
			
			if(game.getGameRule().getPasserTour()){
				passer = new JButton("Passer");
				passer.setBackground(gris);
				passer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
				passer.setPreferredSize(new Dimension(90,30));
				passer.setForeground(new Color(220, 220, 220));
				passer.addActionListener(listener);
				mesBoutons.add(passer);
			}
			
			abandonner = new JButton("Abandonner");
			abandonner.setBackground(gris);
			abandonner.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
			abandonner.setPreferredSize(new Dimension(90,30));
			abandonner.setForeground(new Color(220, 220, 220));
			abandonner.addActionListener(listener);
			mesBoutons.add(abandonner);
			
			mesBoutons.setBackground(grisF);
			this.add(mesBoutons, BorderLayout.SOUTH);
			this.setBorder(BorderFactory.createMatteBorder(5, 7, 7, 5, grisF));
		} else {
			this.setBorder(BorderFactory.createMatteBorder(5, 7, 39, 5, grisF));
		}
	}
	
	/**
	 * Gets the game.
	 *
	 * @return the game
	 */
	public Game getGame(){
		return game;
	}
	
	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public Player getPlayer(){
		return player;
	}
	
	/**
	 * Gets the blokus.
	 *
	 * @return the blokus
	 */
	public Blokus getBlokus(){
		return blokus;
	}
	
	/**
	 * Gets the playerBar.
	 *
	 * @return the playerBar
	 */
	public PlayerBar getBar(){
		return playerBar;
	}
	
	/**
	 * Gets the button passer.
	 *
	 * @return the button passer
	 */
	public JButton getBouttonPasser(){
		return this.passer;
	}
	
	/**
	 * Gets the button abandonner.
	 *
	 * @return the button abandonner
	 */
	public JButton getBouttonAbandonner(){
		return this.abandonner;
	}
	
	/**
	 * Change a few of graphics color to indicate that is the player round.
	 */
	public void isPlaying(){
		playerBar.isPlaying();
		scrollPane.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, player.getColor()));
		if(!(player instanceof IA)){
			if(passer != null) passer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, player.getColor()));
			abandonner.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, player.getColor()));
		}
	}
	
	/**
	 * Change a few of graphics color to indicate that is not the player round.
	 */
	public void isWaiting(){
		Color gris = new Color(59, 59, 59);
		playerBar.isWaiting();
		scrollPane.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, gris));
		if(!(player instanceof IA)){
			if(passer != null) passer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
			abandonner.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
		}
	}
	
	/**
	 * Change a few of graphics color to indicate that the player have finish his game.
	 */
	public void isFinish(){
		Color gris = new Color(59, 59, 59);
		playerBar.isFinish();
		playerPieces.finishPieces();
		scrollPane.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, gris));
		if(!(player instanceof IA)){
			if(passer != null) passer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
			abandonner.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, gris));
		}
		this.revalidate();
	}
	
	/**
	 * Set the player.
	 *
	 * @param p the new player
	 */
	public void setPlayer(Player p){
		this.player = p;
	}
	
	/**
	 * Actualize the playerBar and playerPieces.
	 */
	public void synchro() {
		playerPieces.synchro();
		playerBar.actualiser();
		
		if(!player.getCanPlay()) this.isFinish();
	}
	
	/**
	 * Actualize the playerBar and playerPieces with a new player. (this method is use for the online gamemode)
	 *
	 * @param p the p
	 */
	public void synchro(Player p){
		player = p;
		playerPieces.synchro(p);
		playerBar.setPlayer(p);
		playerBar.actualiser();
	}
	
	/**
	 * Activate the playerPieces.
	 */
	public void activer(){
		this.playerPieces.activer();
	}
	
	
	/**
	 * Desactivate the playerPieces.
	 */
	public void desActiver(){
		this.playerPieces.desActiver();
	}
	
	/**
	 * Hide the button abandonner and passer.
	 */
	public void hideButton(){
		abandonner.setVisible(false);
		if(passer != null) passer.setVisible(false);
	}
}

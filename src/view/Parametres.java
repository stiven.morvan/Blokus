package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import controler.ListenerCards;
import controler.ListenerPlayersMenu;
import model.Pattern;

/**
 * The Class Parametres.
 */
public class Parametres extends JPanel{
	
	/** The pieces. */
	private PlayerPieces pieces;
	
	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	/** The pattern. */
	private Pattern pattern;
	
	/** The liste piece. */
	private int[] listePiece;
	
	/** The index. */
	private int index;
	
	/** The curseur. */
	private JButton curseur;
	
	/** The passer tour. */
	private JButton passerTour;
	
	/** scrollPane. */
	private JScrollPane scrollPane;
	
	/**
	 * Instantiates a new parametres menu.
	 *
	 * @param blokus the blokus
	 */
	public Parametres(Blokus blokus) {
		
		index = 0;
		listePiece = new int[21];
		for (int i = 0; i < 21; i++) {
			listePiece[i] = -1;
		}
		
		this.setLayout(new BorderLayout());
		
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel main = new JPanel();
	    main.setLayout(new BorderLayout());
		
		JLabel sousTitre = new JLabel("Parmetres");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 50));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        ListenerPlayersMenu listener = new ListenerPlayersMenu(2);
        
        JLabel passerTourText  = new JLabel("Passer son tour:");
	    passerTourText.setBackground(gris);
	    passerTourText.setFont(new Font("Gotham Medium", Font.PLAIN, 18));
	    passerTourText.setForeground(new Color(240, 240, 240));
	    passerTourText.setPreferredSize(new Dimension(160, 30));
	    passerTourText.setOpaque(true);
	    
	    passerTour  = new JButton();
	    passerTour.setBackground(new Color(165, 110, 45));
	    passerTour.setPreferredSize(new Dimension(28, 28));
	    passerTour.setOpaque(true);
	    passerTour.setBorder(new LineBorder(grisF, 4, false));
	    passerTour.addActionListener(listener);
	    
	    JPanel passer = new JPanel();
	    passer.setBackground(gris);
	    passer.add(passerTourText);
	    passer.add(passerTour);
	    
	    JLabel curseurText = new JLabel("Curseur avanc�:");
	    curseurText.setBackground(gris);
	    curseurText.setFont(new Font("Gotham Medium", Font.PLAIN, 18));
	    curseurText.setForeground(new Color(240, 240, 240));
	    curseurText.setPreferredSize(new Dimension(160, 30));
	    curseurText.setOpaque(true);
	    
	    JButton curseur  = new JButton();
	    curseur.setBackground(new Color(35, 35, 35));
	    curseur.setPreferredSize(new Dimension(28, 28));
	    curseur.setOpaque(true);
	    curseur.setBorder(new LineBorder(grisF, 4, false));
	    curseur.addActionListener(listener);
	    this.curseur = curseur;
	    
	    JPanel c = new JPanel();
	    c.setBackground(gris);
	    c.add(curseurText);
	    c.add(curseur);
        
        JButton back  = new JButton("Retour");
        back.setBackground(new Color(165, 110, 45));
        back.setMaximumSize(new Dimension(160, 35));
        back.setForeground(Color.WHITE);
        back.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
        back.setOpaque(true);
        back.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        back.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerCards lCard = new ListenerCards(blokus, "Menu");
        back.addActionListener(lCard);

        main.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		main.setBackground(gris);
		main.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
	    
	    pattern = new Pattern();
	    
	    pieces = new PlayerPieces(pattern.getAllCollection(), this);
	    pieces.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, new Color(86, 86, 86)));
	    pieces.setBackground(new Color(86, 86, 86));
	    
	    JPanel section = new JPanel();
	    section.setLayout(new BoxLayout(section, BoxLayout.PAGE_AXIS));
	    section.setBackground(gris);
	    
	    scrollPane = new JScrollPane(pieces);
	    scrollPane.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, gris));
	    scrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
	    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    scrollPane.getVerticalScrollBar().setBackground(gris);
	    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension( scrollPane.getVerticalScrollBar().getPreferredSize().width-7,  scrollPane.getVerticalScrollBar().getPreferredSize().height));
		
	    section.add(sousTitre);
	    section.add(passer);
	    section.add(c);
	    
	    main.add(section, BorderLayout.CENTER);
	    main.add(scrollPane, BorderLayout.WEST);
	    
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
		this.add(back, BorderLayout.SOUTH);
		
		pieces.setPreferredSize(new Dimension(pieces.getPreferredSize().width+80, pieces.getPreferredSize().height+20));
	}
	
	public void setPiece(){
		pattern = new Pattern();
		pieces.refresh((new Pattern().getAllCollection()), this);
	}
	
	/**
	 * Adds a piece for the next game.
	 *
	 * @param i the index of the piece
	 */
	public void addPiece(int i){
		boolean selected = false;
		if (index <= 20) {
			for (int j = 0; j < listePiece.length; j++) {
				if (listePiece[j] == i) {
					selected = true;
				}
			}
			if (!selected) {
				listePiece[index] = i;
				index++;
			}
		}
	}
	
	/**
	 * Removes the piece.
	 *
	 * @param i the index of the piece
	 */
	public void removePiece(int i){
		for (int j = 0; j < listePiece.length; j++) {
			if (listePiece[j] == i) {
				listePiece[j] = -1;
				index--;
			}
		}
	}
	
	/**
	 * Checks if is custom pattern.
	 *
	 * @return true, if is custom pattern
	 */
	public boolean isCustomPattern(){
		int i = 0;
		boolean ret = false;
		while (i < listePiece.length && !ret) {
			if (listePiece[i] != -1) {
				ret = true;
			}
			i++;
		}
		return ret;
	}
	
	/**
	 * Gets the liste of all piece piece.
	 *
	 * @return the liste of piece
	 */
	public int[] getlistePiece(){
		return this.listePiece;
	}
	
	/**
	 * Checks if is dev cursor.
	 *
	 * @return true, if is dev cursor
	 */
	public boolean getCurseur(){
		Color check = new Color(165, 110, 45);
		boolean ret = false;
		if (curseur != null && curseur.getBackground().equals(check)) {
			ret = true;
		}
		return ret;
	}
	
	/**
	 * Checks if skip lap is active.
	 *
	 * @return true if skip lap is active.
	 */
	public boolean getPasserTour(){
		Color check = new Color(165, 110, 45);
		boolean ret = false;
		if (passerTour != null && passerTour.getBackground().equals(check)) {
			ret = true;
		}
		return ret;
	}
}

package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

import controler.Drop;
import controler.Game;
import controler.GridListener;

/**
 * The Class ViewGame is the main view class of a game.
 * It assembly the different playerZone, grid and ZoneInformation panel.
 */
public class ViewGame extends JPanel{
	
	/** The playerZones. */
	private PlayerZone[] playerZones;
	
	/** The zoneInformation. */
	private ZoneInformation zoneInformation;
	
	/** The grid. */
	private Grid grid;
	
	/**
	 * Instantiates a new view game.
	 *
	 * @param blokus the blokus
	 * @param game the game
	 */
	public ViewGame(Blokus blokus, Game game){
		this.setLayout(new BorderLayout());
		
		JPanel gauche = new JPanel();
	    gauche.setBackground(new Color(59, 59, 59));
		JPanel droite = new JPanel();
	    droite.setBackground(new Color(59, 59, 59));
	    
	    int nbJoueur = game.getPlayers().length;
	    playerZones = new PlayerZone[nbJoueur]; 
	    PlayerZone playerZone;
	    if(nbJoueur == 1){
	    	droite.setLayout(new GridLayout(1,1));
	    	playerZone = new PlayerZone(game.getPlayers()[0], game, blokus);
	    	droite.add(playerZone);
	    } else {
	    	int i = 0;
	    	int nbJoueurGauche = nbJoueur / 2;
	    	int nbJoueurDroite = nbJoueurGauche;
	    	
	    	// Si le nombre n'est pas paire il y aura un joueur de plus � gauche
	    	if(nbJoueurGauche != nbJoueur / 2) {
	    		nbJoueurGauche++;
	    	}
	    	gauche.setLayout(new GridLayout(nbJoueurGauche, 1));
	    	while(i < nbJoueurGauche){
		    	playerZone = new PlayerZone(game.getPlayers()[i], game, blokus);
		    	playerZones[i] = playerZone;
	    		gauche.add(playerZone);
	    		i++;
	    	}
	    	droite.setLayout(new GridLayout(nbJoueurDroite, 1));
	    	while(i < nbJoueur){
		    	playerZone = new PlayerZone(game.getPlayers()[i], game, blokus);
		    	playerZones[i] = playerZone;
	    		droite.add(playerZone);
	    		i++;
	    	}
	    }
	    
		grid = new Grid();
		GridListener gl = new GridListener(grid, game);
		grid.setGridListener(gl);
		grid.setTransferHandler(new Drop(game));
		grid.setBackground(new Color(59, 59, 59));
		
		zoneInformation = new ZoneInformation(blokus, game);
		
		this.add(gauche, BorderLayout.WEST);
		this.add(grid, BorderLayout.CENTER);
		this.add(droite, BorderLayout.EAST);
		this.add(zoneInformation, BorderLayout.SOUTH);
		this.setBackground(new Color(59, 59, 59));
	}
	
	/**
	 * Instantiates a new view game.
	 *
	 * @param blokus the blokus
	 * @param game the game
	 * @param cursor if the cursor is advanced
	 */
	public ViewGame(Blokus blokus, Game game, boolean cursor){
    	this(blokus, game);
    	grid.setCursor(cursor);
    }
	
	/**
	 * Get the grid.
	 *
	 * @return the grid
	 */
	public Grid getGrid(){
		return grid;
	}
	
	/**
	 * Gets the playerZones.
	 *
	 * @return the player zones
	 */
	public PlayerZone[] getPlayerZones(){
		return playerZones;
	}
	
	/**
	 * Gets the zoneInformation.
	 *
	 * @return the zone information
	 */
	public ZoneInformation getZoneInformation(){
		return this.zoneInformation;
	}
}
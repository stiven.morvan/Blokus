package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The Class Blokus is the frame who contain the game blokus. It's also the launcher of this game.
 */
public class Blokus extends JFrame {
	
	/** The jPanel who have the cardLayout. */
	private JPanel cards;
	
	/** The cardLayout who contains the different cards. */
	private CardLayout cl;
	
	/** The view game, the jPanel who displays the actual game (the grid and the playerZone).*/
	private ViewGame viewGame;
	
	/** The glassPane used to animate the drag and drop of the pieces with a transparent piece.*/
	private GlassPane glass;
	
	/** The main menu where we can access to all different menu.*/
	private MainMenu main;
	
	/** The parametres menu where we can change the parameter of the game.*/
	private Parametres parametres;
	
	/**
	 * The main method, who launch this program.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new Blokus();
	}
	
	/**
	 * Instantiates a new blokus frame.
	 */
	public Blokus(){
		super("Blokus");
		this.setLocation(200,200);
		this.setSize(1250, 730);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// GlassPane utiliser pour l'effet de drag & drop
		glass = new GlassPane();
		this.setGlassPane(glass);
		
		cards = new JPanel(new CardLayout());
		
		this.main = new MainMenu(this);
		cards.add(main, "Main");
		
		parametres = new Parametres(this);
		cards.add(parametres, "Parametres");

		MenuCreation menu = new MenuCreation(this, parametres, false);
		cards.add(menu, "Menu");
		
		MenuPiece crea = new MenuPiece(this);
		cards.add(crea, "MenuCrea");
		
		Aide aide = new Aide(this);
		cards.add(aide, "Aide");
		
		MenuLan lan = new MenuLan(this);
		cards.add(lan, "Lan");
		
		this.getContentPane().add(cards, BorderLayout.CENTER);
		cl = (CardLayout)(cards.getLayout());
		cl.show(cards, "Main");
		
		this.setVisible(true);
	}
	
	/**
	 * Gets the main menu of the blokus.
	 *
	 * @return the main menu (a jPanel)
	 */
	public MainMenu getMain() {
		return main;
	}
	
	/**
	 * Adds the jPanel to the cardLayout with the name in parameters.
	 *
	 * @param name the name of the jPanel
	 * @param p the jPanel
	 */
	public void addCard(String name, JPanel p){
		cards.add(p, name);
	}
	
	/**
	 * Gets the jPanel who have the cardLayout.
	 *
	 * @return the cards
	 */
	public JPanel getCards() {
		return cards;
	}

	/**
	 * Sets the jPanel who have the cardLayout.
	 *
	 * @param cards the new cards
	 */
	public void setCards(JPanel cards) {
		this.cards = cards;
	}
	
	/**
	 * Show the jPanel in cards, identified by the name in parameters.
	 *
	 * @param s the s
	 */
	public void show(String s){
		cl.show(cards, s);
	}
	
	/**
	 * Sets the viewGame.
	 *
	 * @param vg the new viewGame
	 */
	public void setViewGame(ViewGame vg){
		this.addCard("game", vg);
		this.viewGame = vg;
	}
	
	/**
	 * Gets the viewGame.
	 *
	 * @return the viewGame
	 */
	public ViewGame getViewGame(){
		return viewGame;
	}
	
	/**
	 * Removes the actual game.
	 */
	public void removeGame(){
		cards.remove(viewGame);
		viewGame = null;
	}
	
	/**
	 * Gets the glassPane.
	 *
	 * @return the glass
	 */
	public GlassPane getGlass(){
		return glass;
	}
	
	/**
	 * Gets the parametres.
	 *
	 * @return the parametres
	 */
	public Parametres getParametres() {
		return this.parametres;
	}
}
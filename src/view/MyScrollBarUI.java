package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 * This class stylish the scrollBar
 */
public class MyScrollBarUI extends BasicScrollBarUI{
	
	/** The three color used in the scrollBar. */
	Color one, two, three;
	
	/**
	 * Instantiates a new custom scrollBar.
	 */
	public MyScrollBarUI(){
		one = new Color(55, 55, 55);
		two = new Color(40, 40, 40);
		three = new Color(77, 77, 77);
	}
 
	/**
	 * Make invisible the decrease button.
	 * 
	 * @param orientation the orientation.
	 * @return JButton
	 */
	@Override
	protected JButton createDecreaseButton(int orientation) {
		JButton button = new JButton();
	    button.setBackground(two);
	    button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, two));
	    Dimension zeroDim = new Dimension(0,0);
	    button.setPreferredSize(zeroDim);
	    button.setMinimumSize(zeroDim);
	    button.setMaximumSize(zeroDim);
	    return button;
	}
 
	/**
	 * Make invisible the increase button.
	 * 
	 * @param orientation the orientation.
	 * @return JButton
	 */
	@Override
	protected JButton createIncreaseButton(int orientation) {
		JButton button = new JButton();
	    button.setBackground(two);
	    button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, two));
	    Dimension zeroDim = new Dimension(0,0);
	    button.setPreferredSize(zeroDim);
	    button.setMinimumSize(zeroDim);
	    button.setMaximumSize(zeroDim);
	    return button;
	}
	
	/**
	 * Change the color of the track.
	 * 
	 * @param g the Graphics object.
	 * @param c the JComponent.
	 * @param Rectangle.
	 */
	@Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        g.setColor(three);
        g.fillRect(0, 0, 20, 130);
    }
	
	/**
	 * Change the color and the size of the thumb.
	 * 
	 * @param g the Graphics object.
	 * @param c the JComponent.
	 * @param Rectangle.
	 */
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        g.setColor(one);
        g.translate(thumbBounds.x, thumbBounds.y);
        g.fillRect(0, 0, thumbBounds.width, thumbBounds.height);
        g.translate(-thumbBounds.x, -thumbBounds.y);
    }
}

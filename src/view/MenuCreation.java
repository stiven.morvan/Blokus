package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controler.ListenerCards;
import controler.ListenerModeMenu;
import controler.ListenerPlayersMenu;
import controler.ListenerStart;
import controler.ListenerStartOnline;

/**
 * The Class MenuCreation is a jPanel where the user can define all player of the game.
 */
public class MenuCreation extends JPanel{
	
	/** The LinePlayer 1. */
	private LinePlayer p1;
	
	/** The LinePlayer 2. */
	private LinePlayer p2;
    
    /** The LinePlayer 3. */
    private LinePlayer p3;
    
    /** The LinePlayer 4. */
    private LinePlayer p4;
    
    /** The blokus frame. */
    private Blokus blokus;
	
	/**
	 * Instantiates a new menu creation.
	 *
	 * @param blokus the blokus frame
	 * @param parametres the parameters of the futur game
	 * @param online if the game is online
	 */
	public MenuCreation(Blokus blokus, Parametres parametres, boolean online){
		this.setLayout(new BorderLayout());
		this.blokus = blokus;
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel main = new JPanel();
	    main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JLabel sousTitre = new JLabel("Creation d'une partie");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 50));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        ListenerPlayersMenu lPlayersCouleur = new ListenerPlayersMenu(1);
        
        ListenerPlayersMenu lPlayersIa1 = new ListenerPlayersMenu(2);
        ListenerPlayersMenu lPlayersIa2 = new ListenerPlayersMenu(2);
        p1 = new LinePlayer("Joueur1", lPlayersIa1.getColor(0), lPlayersCouleur, lPlayersIa1, online);
        lPlayersIa1.setLine(p1);
        p2 = new LinePlayer("Joueur2", lPlayersIa1.getColor(1), lPlayersCouleur, lPlayersIa2, online);
        lPlayersIa2.setLine(p2);
        JPanel player1 = p1.getLine();
        JPanel player2 = p2.getLine();
        JPanel player3 = null;
        JPanel player4 = null;
        JButton addPlayer = null;
        if(!online){
        	ListenerPlayersMenu lPlayersIa3 = new ListenerPlayersMenu(2);
        	ListenerPlayersMenu lPlayersIa4 = new ListenerPlayersMenu(2);
		    p3 = new LinePlayer("Joueur3", lPlayersIa1.getColor(2), lPlayersCouleur, lPlayersIa3, online);
            lPlayersIa3.setLine(p3);
        	p4 = new LinePlayer("Joueur4", lPlayersIa1.getColor(3), lPlayersCouleur, lPlayersIa4, online);
            lPlayersIa4.setLine(p4);
            player3 = p3.getLine();
            player4 = p4.getLine();
            player3.setVisible(false);
            player4.setVisible(false);
            
            ListenerModeMenu lPlayersMode = new ListenerModeMenu(main, player3, player4);
            
            addPlayer  = new JButton("Mode 4 joueur");
            addPlayer.setBackground(grisF);
            addPlayer.setPreferredSize(new Dimension(200, 35));
            addPlayer.setForeground(new Color(240, 240, 240));
            addPlayer.setFont(new Font("Gotham Medium", Font.PLAIN, 19));
            addPlayer.setOpaque(true);
            addPlayer.setBorder(BorderFactory.createEmptyBorder(6,23,6,23));
            addPlayer.addActionListener(lPlayersMode);
            addPlayer.setAlignmentX(JButton.CENTER_ALIGNMENT);
        }
        
        ListenerPlayersMenu optionListener = new ListenerPlayersMenu(2);
        
        JPanel option = null;
        if(!online){
        	option = optionLine(optionListener);
        }
        
        JButton launch  = new JButton("Lancer la Partie!");
        launch.setBackground(new Color(165, 110, 45));
        launch.setMaximumSize(new Dimension(240, 45));
        launch.setForeground(Color.WHITE);
        launch.setFont(new Font("Gotham Medium", Font.PLAIN, 22));
        launch.setOpaque(true);
        launch.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        launch.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerStart ls = null;
        if(!online){
        	ls = new ListenerStart(p1, p2, p3, p4, parametres, blokus);
        } else {
        	ls = new ListenerStartOnline(p1, p2, p3, p4, blokus);
        }
        launch.addActionListener(ls);

 		JButton back  = new JButton("Retour");
        back.setBackground(new Color(165, 110, 45));
        back.setMaximumSize(new Dimension(160, 35));
        back.setForeground(Color.WHITE);
        back.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
        back.setOpaque(true);
        back.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        back.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerCards lCard = new ListenerCards(blokus, "Main");
        back.addActionListener(lCard);

        main.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		main.setBackground(gris);
		main.setBorder(BorderFactory.createEmptyBorder(0,0,180,0));
		main.setMaximumSize(new Dimension(100, 100));
		main.setPreferredSize(new Dimension(100, 100));
		main.setSize(new Dimension(100, 100));
				
        
		main.add(sousTitre);
		main.add(player1);
		main.add(player2);
		if(!online){
			main.add(player3);
			main.add(player4);
			main.add(addPlayer);
			main.add(option);
		}
		main.add(launch);
        
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
		this.add(back, BorderLayout.SOUTH);
	}
	
	/**
	 * Get the LinePlayer 1.
	 *
	 * @return the p1
	 */
	public LinePlayer getP1() {
		return p1;
	}

	/**
	 * Sets the LinePlayer 1.
	 *
	 * @param p1 the new p1
	 */
	public void setP1(LinePlayer p1) {
		this.p1 = p1;
	}

	/**
	 * Get the LinePlayer 2.
	 *
	 * @return the p2
	 */
	public LinePlayer getP2() {
		return p2;
	}

	/**
	 * Sets the LinePlayer 2.
	 *
	 * @param p2 the new p2
	 */
	public void setP2(LinePlayer p2) {
		this.p2 = p2;
	}

	/**
	 * Get the LinePlayer 3.
	 *
	 * @return the p3
	 */
	public LinePlayer getP3() {
		return p3;
	}

	/**
	 * Sets the LinePlayer 3.
	 *
	 * @param p3 the new p3
	 */
	public void setP3(LinePlayer p3) {
		this.p3 = p3;
	}

	/**
	 * Get the LinePlayer 4.
	 *
	 * @return the p4
	 */
	public LinePlayer getP4() {
		return p4;
	}

	/**
	 * Sets the LinePlayer 4.
	 *
	 * @param p4 the new p4
	 */
	public void setP4(LinePlayer p4) {
		this.p4 = p4;
	}

	/**
	 * Return the button parametres who can display the parameters.
	 *
	 * @param listener the listener
	 * @return the j panel
	 */
	private JPanel optionLine(ListenerPlayersMenu listener){
		Color gris = new Color(77, 77, 77);
		Color grisF = new Color(59, 59, 59);
		JPanel line = new JPanel();
	    line.setLayout(new FlowLayout());
	    
	    ListenerCards lc = new ListenerCards(blokus, "Parametres");
	    
	    JButton parametre  = new JButton("Parametres");
        parametre.setBackground(grisF);
        parametre.setPreferredSize(new Dimension(180, 30));
        parametre.setForeground(new Color(240, 240, 240));
        parametre.setFont(new Font("Gotham Medium", Font.PLAIN, 17));
        parametre.setOpaque(true);
        parametre.setBorder(BorderFactory.createEmptyBorder(6,23,6,23));
        parametre.addActionListener(lc);
        parametre.setAlignmentX(JButton.CENTER_ALIGNMENT);
	    
	    line.setBackground(gris);
	    line.setBorder(BorderFactory.createEmptyBorder(20,0,40,0));
	    
	    JPanel center = new JPanel();
	    center.setLayout(new GridLayout(1, 0));
	   
	    line.add(parametre);
	    line.setMaximumSize(new Dimension(500, 350));
	    line.setAlignmentY(Component.BOTTOM_ALIGNMENT);
	    
		return line;
	}	
}

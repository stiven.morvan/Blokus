package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import controler.ListenerCards;
import controler.MainButtonListener;

/**
 * The Class MainMenu is a jPanel who represent the menu where the player can access to all other menu.
 */
public class MainMenu extends JPanel{
	
	/**
	 * Instantiates a new main menu.
	 *
	 * @param blokus the blokus frame
	 */
	public MainMenu(Blokus blokus){
		this.setLayout(new BorderLayout());
		
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JPanel button = new JPanel();
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel sousTitre = new JLabel("Menu principale");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 50));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
		Border borderButton = new LineBorder(gris, 30);
		
		JButton jouer = new JButton(new ImageIcon("data/icons/start.png"));
		jouer.setBackground(gris);
		jouer.setPreferredSize(new Dimension(260, 260));
		jouer.setBorder(borderButton);
		ListenerCards lPlay = new ListenerCards(blokus, "Menu","start");
		jouer.addActionListener(lPlay);
		jouer.addMouseListener(lPlay);
		button.add(jouer);
		
		JButton lan = new JButton(new ImageIcon("data/icons/lan.png"));
		lan.setBackground(gris);
		lan.setPreferredSize(new Dimension(260, 260));
		lan.setBorder(borderButton);
		ListenerCards lLan = new ListenerCards(blokus, "Lan", "lan");
		lan.addActionListener(lLan);
		lan.addMouseListener(lLan);
		button.add(lan);
		
		JButton crea = new JButton(new ImageIcon("data/icons/crea.png"));
		crea.setBackground(gris);
		crea.setPreferredSize(new Dimension(260, 260));
		crea.setBorder(borderButton);
		ListenerCards lCrea = new ListenerCards(blokus, "MenuCrea", "crea");
		crea.addActionListener(lCrea);
		crea.addMouseListener(lCrea);
		button.add(crea);
		
		JButton aide = new JButton(new ImageIcon("data/icons/aide.png"));
		aide.setBackground(gris);
		aide.setPreferredSize(new Dimension(260, 260));
		aide.setBorder(borderButton);
		ListenerCards lAide = new ListenerCards(blokus, "Aide", "aide");
		aide.addActionListener(lAide);
		aide.addMouseListener(lAide);
		button.add(aide);
		
		JLabel sauvegardeText = new JLabel("Reprendre une sauvegarde:");
	    sauvegardeText.setBackground(gris);
	    sauvegardeText.setFont(new Font("Gotham Medium", Font.PLAIN, 18));
	    sauvegardeText.setForeground(new Color(240, 240, 240));
	    sauvegardeText.setPreferredSize(new Dimension(265, 30));
	    sauvegardeText.setOpaque(true);
		
		JTextField sauvegarde = new JTextField(15);
		sauvegarde.setPreferredSize(new Dimension(200, 37));
		sauvegarde.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
		sauvegarde.setForeground(new Color(240, 240, 240));
		sauvegarde.setBorder(new LineBorder(grisF, 2, false));
		sauvegarde.setText("Nom de la sauvegarde");
		sauvegarde.setBackground(new Color(70, 70, 70));
		MainButtonListener lSauvegarde = new MainButtonListener(blokus);
		sauvegarde.addActionListener(lSauvegarde);
		
		JPanel save = new JPanel();
		save.add(sauvegardeText);
		save.add(sauvegarde);
		save.setBackground(gris);
	    
		button.setBorder(new LineBorder(gris, 5));
		button.setBackground(gris);
		
		main.setBorder(new LineBorder(gris, 10));
		main.setBackground(gris);
		
		main.add(sousTitre);
		main.add(button);
		main.add(save);
		
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
	}
}

package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controler.ListenerCards;
import controler.ListenerCreationPiece;
import model.Pattern;
import model.Position;


/**
 * The Class MenuPiece is the menu where the user can create a new piece.
 */
public class MenuPiece extends JPanel{
	
	/** The jLabelTab. */
	private JLabel[][] jLabelTab;
	
	/** The piece board. */
	private int[][] piece;
	
	/** The pieces display (view). */
	private PlayerPieces pieces;
	
	/** The pattern. */
	private Pattern pattern;
	
	/** The blokus. */
	private Blokus blokus;
	
	/**
	 * Instantiates a new menu piece.
	 *
	 * @param blokus the blokus
	 */
	public MenuPiece(Blokus blokus){
		this.blokus = blokus;
		
		piece = new int[5][5];
		this.setLayout(new BorderLayout());
		
		Color grisF = new Color(59, 59, 59);
		Color gris = new Color(77, 77, 77);
		
		JLabel titre = new JLabel("BLOKUS");
		titre.setBackground(grisF);
		titre.setForeground(new Color(220, 220, 220));
		titre.setOpaque(true);
		titre.setBorder(new LineBorder(grisF, 30));
		titre.setFont(new Font("Gotham Medium", Font.PLAIN, 40));
		titre.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel main = new JPanel();
	    main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		JLabel sousTitre = new JLabel("Creation d'une nouvelle piece");
        sousTitre.setBackground(gris);
        sousTitre.setForeground(new Color(240, 240, 240));
        sousTitre.setOpaque(true);
        sousTitre.setBorder(new LineBorder(gris, 50));
        sousTitre.setFont(new Font("Gotham Medium", Font.PLAIN, 26));
        sousTitre.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        
        ListenerCreationPiece lJLabel = new ListenerCreationPiece(this);
        
        JButton add  = new JButton("Ajouter la piece");
        add.setBackground(new Color(165, 110, 45));
        add.setMaximumSize(new Dimension(248, 45));
        add.setForeground(Color.WHITE);
        add.setFont(new Font("Gotham Medium", Font.PLAIN, 22));
        add.setOpaque(true);
        add.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        add.setAlignmentX(JButton.CENTER_ALIGNMENT);
        add.addActionListener(lJLabel);
        
        JButton back  = new JButton("Retour");
        back.setBackground(new Color(165, 110, 45));
        back.setMaximumSize(new Dimension(160, 35));
        back.setForeground(Color.WHITE);
        back.setFont(new Font("Gotham Medium", Font.PLAIN, 15));
        back.setOpaque(true);
        back.setBorder(BorderFactory.createEmptyBorder(13,23,10,23));
        back.setAlignmentX(JButton.CENTER_ALIGNMENT);
        ListenerCards lCard = new ListenerCards(blokus, "Main");
        back.addActionListener(lCard);

        main.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		main.setBackground(gris);
		main.setBorder(BorderFactory.createEmptyBorder(0,0,180,0));
		main.setMaximumSize(new Dimension(100, 100));
		main.setPreferredSize(new Dimension(100, 100));
		main.setSize(new Dimension(100, 100));
				
		JPanel grid = new JPanel();
		grid.setLayout(new GridLayout(5,5));
	    grid.setSize(250, 260);
	    grid.setMaximumSize(new Dimension(250, 260));
	    grid.setMinimumSize(new Dimension(250, 260));
	    grid.setPreferredSize(new Dimension(250, 260));
	    grid.setBackground(gris);
	    jLabelTab = new JLabel[5][5];
	    
	    LineBorder border = new LineBorder(gris, 1);
	    
	    for (int x = 0; x < 5; x++) {
	    	for (int y = 0; y < 5; y++) {
				JLabel label = new JLabel("");
				jLabelTab[x][y] = label;
				label.setBackground(new Color(20, 20, 20));
				label.setOpaque(true);
				label.setBorder(border);
				
				label.addMouseListener(lJLabel);
				grid.add(label);
			}
		}
	    
	    pattern = new Pattern();
	    
	    pieces = new PlayerPieces(pattern.getAllCollection());
	    
	    JPanel section = new JPanel();
	    section.setLayout(new GridLayout(0, 2));
	    
	    JPanel zoneDeCrea = new JPanel();
	    zoneDeCrea.setLayout(new BoxLayout(zoneDeCrea, BoxLayout.PAGE_AXIS));
	    zoneDeCrea.setAlignmentX(JLabel.CENTER_ALIGNMENT);
	    zoneDeCrea.setBackground(gris);
	    zoneDeCrea.setBorder(BorderFactory.createEmptyBorder(0,0,180,0));
	    
	    zoneDeCrea.add(grid);
	    zoneDeCrea.add(add);
	    
	    section.add(zoneDeCrea);
	    section.add(pieces);

	    main.add(sousTitre);
		main.add(section);
        
		this.add(titre, BorderLayout.NORTH);
		this.add(main, BorderLayout.CENTER);
		this.add(back, BorderLayout.SOUTH);
	}
	
	/**
	 * Gets the piece board.
	 *
	 * @return the piece
	 */
	public int[][] getPiece() {
		return piece;
	}

	/**
	 * Gets the jLabelTab.
	 *
	 * @return the jLabelTab
	 */
	public JLabel[][] getjLabelTab() {
		return jLabelTab;
	}
	
	/**
	 * Adds a hover effect(the actual jLabel where the cursor is have its border changed).
	 *
	 * @param p the position of the cursor
	 */
	public void addHover(Position p){
		jLabelTab[p.getY()][p.getX()].setBorder(new LineBorder(Color.ORANGE, 1));
	}
	
	/**
	 * Removes the hover effect.
	 *
	 * @param p the position of the hover effect
	 */
	public void removeHover(Position p){
		jLabelTab[p.getY()][p.getX()].setBorder(new LineBorder(new Color(77, 77, 77), 1));
	}
	
	/**
	 * Add a part of the piece on the screen and on the futur piece.
	 *
	 * @param p the position
	 */
	public void add(Position p){
		jLabelTab[p.getY()][p.getX()].setBackground(new Color(165, 110, 45));
		piece[p.getX()][p.getY()] = 1;
	}
	
	/**
	 * Removes a part of the piece.
	 *
	 * @param p the position
	 */
	public void remove(Position p){
		jLabelTab[p.getY()][p.getX()].setBackground(new Color(20, 20, 20));
		piece[p.getX()][p.getY()] = 0;
	}
	
	/**
	 * Refresh the pattern.
	 */
	public void refresh(){
		pattern = new Pattern();
		pieces.refresh(pattern.getAllCollection(), null);
	}
	
	/**
	 * Delete all part of the futur piece.
	 */
	public void flush(){
		for (int x = 0; x < 5; x++) {
	    	for (int y = 0; y < 5; y++) {
				jLabelTab[x][y].setBackground(new Color(20, 20, 20));
				piece[x][y] = 0;
			}
		}
	}
	
	/**
	 * Get Blokus.
	 *
	 * @return blokus the blokus.
	 */
	public Blokus getBlokus() {
		return blokus;
	}
}

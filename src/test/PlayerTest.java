package test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Pattern;
import model.Piece;
import model.Player;

public class PlayerTest {

	@Test
	public final void testUsePiece() {
		Player player = new Player(Color.BLUE, "test");
		int r = (int)(Math.random()*player.PATTERN_NUMBER);
		Piece p = player.getPieces()[r];
		int nbPiece = player.getActualNbPiece();
		
		player.usePiece(p);
		
		assertTrue("Piece contenu dans player isUse", p.getIsUse());
		assertTrue("Piece contenu dans player ActualNbPiece", player.getActualNbPiece() == (nbPiece-1));
		
		player.usePiece(null);
		
		assertTrue("Piece null", player.getActualNbPiece() == nbPiece-1);
		
		player.usePiece(new Piece(25, new Pattern(), player));
		
		assertTrue("Piece null", player.getActualNbPiece() == nbPiece-1);
	}
	
	@Test
	public final void testPlayer() {
		Player player = new Player(Color.BLUE, "test");
		
		assertTrue("Piece 2 parametre", player.getActualNbPiece() >= 0 && player.getPATTERN_NUMBER() == player.getActualNbPiece());
		
		Pattern pattern = new Pattern();
		player = new Player(Color.BLUE, "test", pattern);
		
		assertTrue("Piece 3 parametre", player.getPattern() == pattern);
	}

}

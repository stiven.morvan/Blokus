package test;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import model.GameRuleOnline;
import model.PlayerOnline;

public class PlayerOnlineTest {

	@Test
	public void testPlayerOnline() {
		PlayerOnline player = new PlayerOnline( Color.BLUE, "1", 21, 42, 5);
		assertTrue("1 parametre", player.getId() == 5);
	}
}

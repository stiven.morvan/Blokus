package test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Position;

public class PositionTest {

	@Test
	public void testPosition() {
		Position p1 = new Position();
		
		assertTrue("Sans parametres", p1.getX() == 0 && p1.getY() == 0);
		
		int x = (int)(Math.random()*100);
		int y = (int)(Math.random()*100);
		Position p2 = new Position(x, y);
		
		assertTrue("Parametres random", p2.getX() == x && p2.getY() == y);
	}
	
	@Test
	public void testGetYSetY() {
		Position p1 = new Position();
		int y = (int)(Math.random()*100);
		p1.setY(y);
		assertTrue("random positif", p1.getY() == y);
		
		y = (int)(Math.random()*(-100));
		p1.setY(y);
		assertTrue("random negatif", p1.getY() == y);
		
		y = 0;
		p1.setY(y);
		assertTrue("null", p1.getY() == y);
	}
	
	@Test
	public void testGetXSetX() {
		Position p1 = new Position();
		int x = (int)(Math.random()*100);
		p1.setX(x);
		assertTrue("random positif", p1.getX() == x);
		
		x = (int)(Math.random()*(-100));
		p1.setX(x);
		assertTrue("random negatif", p1.getX() == x);
		
		x = 0;
		p1.setX(x);
		assertTrue("null", p1.getX() == x);
	}

}

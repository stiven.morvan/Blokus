package test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.GameRule;
import model.Player;

public class GameRuleTest {
	
	private Player[] players;

	@Before
	public void setUp() throws Exception {
		players = new Player[4];
		players[0] = new Player(Color.BLUE, "1");
		players[1] = new Player(Color.BLUE, "2");
		players[2] = new Player(Color.BLUE, "3");
		players[3] = new Player(Color.BLUE, "4");
	}

	@Test
	public void testGameRule() {
		GameRule gr = new GameRule(players);
		
		assertTrue("1 parametre", gr.getPlayers().length == 4 && 3 == gr.getActualPlayer());
		
		gr = new GameRule(new Player[0], true);
		
		assertTrue("2 Parametres (true)", gr.getPasserTour() == true);
		
		gr = new GameRule(new Player[4], false);
		
		assertTrue("2 Parametres (false)", gr.getPasserTour() == false);
	}
	
	@Test
	public void testSetRandom() {
		GameRule gr = new GameRule(players);
		assertTrue("1 parametre SetOrigin player", players[0] != null && players[1] != null && players[2] != null && players[3] != null);
		
	}
	
	@Test
	public void testGetScoreTab() {
		GameRule gr = new GameRule(players);
		Player[] p = gr.getScoreTab();
		assertTrue("getScoreTab", p != null && p.length == gr.getPlayers().length);
	}
}

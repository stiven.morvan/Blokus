package test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import model.GameRuleOnline;
import model.Player;
import model.PlayerOnline;

public class GameRuleOnlineTest {
	private PlayerOnline[] players;

	@Before
	public void setUp() throws Exception {
		players = new PlayerOnline[4];
		players[0] = new PlayerOnline( Color.BLUE, "1", 21, 42, 0);
		players[1] = new PlayerOnline( Color.BLUE, "2", 21, 42, 1);
		players[2] = new PlayerOnline( Color.BLUE, "3", 21, 42, 2);
		players[3] = new PlayerOnline( Color.BLUE, "4", 21, 42, 3);
	}

	@Test
	public void testGameRuleOnline() {
		GameRuleOnline gr = new GameRuleOnline(players);
		
		assertTrue("1 parametre", gr.getPlayers().length == players.length && 0 == gr.getVersion());
	}

	@Test
	public void testGetPlayerById() {
		GameRuleOnline gr = new GameRuleOnline(players);
		
		PlayerOnline p = (PlayerOnline) gr.getPlayers()[3];
		assertTrue("1 parametre", gr.getPlayerById(p).getId() == p.getId());

		p = (PlayerOnline) gr.getPlayers()[1];
		assertTrue("2 parametre", gr.getPlayerById(p).getId() == p.getId());
	}
}

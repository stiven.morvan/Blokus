package test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.Test;

import model.Pattern;
import model.Piece;
import model.Player;

public class PieceTest {
	
	@Test
	public void testPiece() {
		int rand = (int)(Math.random()*20);
		Pattern pattern = new Pattern();
		Piece p = new Piece(rand, pattern);
		
		assertTrue("Piece Pattern ini", p.getPattern() != null);
		assertTrue("Piece isUse ini", p.getIsUse() == false);
		
		rand = (int)(Math.random()*20);
		Player player = new Player(Color.BLUE, "test");
		p = new Piece(rand, pattern, player);
		
		assertTrue("Piece Pattern ini", p.getPattern() != null);
		assertTrue("Piece isUse ini", p.getIsUse() == false);
		assertTrue("Piece Player ini", p.getPlayer() == player);
	}
	
	@Test
	public void testSetScore() {
		Piece p = new Piece(0, new Pattern());
		p.setScore();
		
		assertTrue("piece num1 (1x1)", p.getScore() == 1);
		
		p = new Piece(1, new Pattern());
		p.setScore();
		
		assertTrue("piece num2 (2x1)", p.getScore() == 2);
		
		p = new Piece(2, new Pattern());
		p.setScore();
		
		assertTrue("piece num3", p.getScore() == 3);
		
		p = new Piece(20, new Pattern());
		p.setScore();
		
		assertTrue("piece num20", p.getScore() == 5);
	}

}

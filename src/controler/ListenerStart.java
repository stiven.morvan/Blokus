package controler;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.GameRule;
import model.IA;
import model.Pattern;
import model.Player;
import view.Blokus;
import view.LinePlayer;
import view.Parametres;
import view.ViewGame;

/**
 * The Class ListenerStart.
 */
public class ListenerStart  implements ActionListener{
	
	/** The lines. */
	protected LinePlayer[] lines;
	
	/** The players. */
	protected Player[] players;
	
	/** The blokus. */
	protected Blokus blokus;
	
	/** The parametres. */
	private Parametres parametres;
	
	/**
	 * Instantiates a new listener start.
	 *
	 * @param player1 the player 1
	 * @param player2 the player 2
	 * @param player3 the player 3
	 * @param player4 the player 4
	 * @param blokus the blokus
	 */
	public ListenerStart(LinePlayer player1, LinePlayer player2, LinePlayer player3, LinePlayer player4, Blokus blokus){
		players = new Player[4];
		lines = new LinePlayer[4];
		lines[0] = player1;
		lines[1] = player2;
		lines[2] = player3;
		lines[3] = player4;
		this.blokus = blokus;
	}
	
	/**
	 * Instantiates a new listener start.
	 *
	 * @param player1 the player 1
	 * @param player2 the player 2
	 * @param player3 the player 3
	 * @param player4 the player 4
	 * @param parametres the parametres
	 * @param blokus the blokus
	 */
	public ListenerStart(LinePlayer player1, LinePlayer player2, LinePlayer player3, LinePlayer player4, Parametres parametres, Blokus blokus){
		this(player1, player2, player3, player4, blokus);
		this.parametres = parametres;
	}

	/**
	 * Start and initialize the game.
	 *
	 * @param e the action event
	 */
	public void actionPerformed(ActionEvent e) {
		
		for (int i = 0; i < players.length; i++) {
			players[i] = null;
		}
		if (lines[0].getLine().isVisible()) {
			players[0] = getPlayerObject(lines[0]);
			if (lines[1].getLine().isVisible()) {
				players[1] = getPlayerObject(lines[1]);
				if (lines[2].getLine().isVisible()) {
					players[2] = getPlayerObject(lines[2]);
					if (lines[3].getLine().isVisible()) {
						players[3] = getPlayerObject(lines[3]);
					}
				}
			}
		}

		GameRule gr;
		if(parametres.getPasserTour()){
			gr = new GameRule(players);
		} else {
			gr = new GameRule(players, false);
		}
		Game game = new Game(gr, blokus); 
		blokus.setViewGame( new ViewGame(blokus, game, parametres.getCurseur()));
		game.nextTour();
		blokus.show("game");
	}
	
	/**
	 * Gets the player object from a playerLine.
	 *
	 * @param p a playerLine
	 * @return the player object
	 */
	protected Player getPlayerObject(LinePlayer p){
		Pattern pattern = new Pattern();
		Player ret = null;
		if (parametres.isCustomPattern()) {
			if (p != null) {
				if (p.getIa()) {
					
					ret = new IA(p.getCouleur().getBackground(), p.getNom().getText(), pattern, parametres.getlistePiece(), p.getLevel());
				}
				else{
					ret = new Player(p.getCouleur().getBackground(), p.getNom().getText(), pattern, parametres.getlistePiece());
				}
			}
			else{
				ret = new Player(Color.RED, "ERREUR");
			}
		}
		else{
			if (p != null) {
				if (p.getIa()) {
					ret = new IA(p.getCouleur().getBackground(), p.getNom().getText(), p.getLevel());
				}
				else{
					ret = new Player(p.getCouleur().getBackground(), p.getNom().getText());
				}
			}
			else{
				ret = new Player(Color.RED, "ERREUR");
			}
		}
		return ret;
	}
}

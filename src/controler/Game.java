package controler;

import model.GameRule;
import model.IA;
import model.Map;
import model.Piece;
import model.Player;
import model.PlayerOnline;
import model.Position;
import view.Blokus;
import view.Grid;
import view.PlayerPiece;
import view.PlayerZone;

/**
 * The Class Game is the main class use for the functionment of the game. This class make the connection beetwen the model and the view.
 */
public class Game {
	
	/** The GameRule of this game. */
	protected GameRule gr;
	
	/** The blokus, the frame who contains and display the game. */
	protected Blokus blokus;
	
	/** The next piece to place. */
	protected Piece nextPiece;
	
	/** The last piece to place (in view). */
	protected PlayerPiece lastPiece;
	
	/** The next position of the nextPiece (it's the actual position of the cursor in the grid). */
	protected Position nextPosition;
	
	/**
	 * Instantiates a new game.
	 *
	 * @param gr the GameRule of this game
	 * @param blokus the frame who contains and display the game.
	 */
	public Game(GameRule gr, Blokus blokus){
		this.gr = gr;
		this.blokus = blokus;
		nextPosition = null;
		nextPiece = null;
		lastPiece = null;
	}
	
	/**
	 * Synchronize this game (in view) with its GameRule. (used in the only mode and when we restart a game)
	 */
	public void synchro(){
		Piece[][] map =	gr.getMap().getMap();
		for (int y = 0; y < map.length; y++) {
			for (int x = 0; x < map[y].length; x++) {
				if(map[y][x] != null){
					blokus.getViewGame().getGrid().addFragmentOfPiece(map[y][x], new Position(x,y));
				}
			}
		}
		for(PlayerZone pz : blokus.getViewGame().getPlayerZones()){
			pz.synchro();
		}
	}
	
	/**
	 * Gets the actual grid.
	 *
	 * @return the grid
	 */
	public Grid getGrid(){
		return blokus.getViewGame().getGrid();
	}
	
	/**
	 * Gets the players of this game.
	 *
	 * @return the players
	 */
	public Player[] getPlayers(){
		return this.gr.getPlayers();
	}
	
	/**
	 * Gets the actual player.
	 *
	 * @return the actual player
	 */
	public Player getActualPlayer(){
		Player p = null;
		if(gr.getActualPlayer() != -1){
			p = gr.getPlayers()[gr.getActualPlayer()];
		}
		return p;
	}
	
	/**
	 * Gets the last playerPiece (in view)
	 *
	 * @return the last piece
	 */
	public PlayerPiece getLastPiece(){
		return lastPiece;
	}
	
	/**
	 * Sets the last playerPiece (in view).
	 *
	 * @param piece the new last piece
	 */
	public void setLastPiece(PlayerPiece piece){
		lastPiece = piece;
	}
	
	/**
	 * Gets the next piece to place.
	 *
	 * @return the next piece
	 */
	public Piece getNextPiece() {
		return nextPiece;
	}

	/**
	 * Gets the next position.
	 *
	 * @return the next position
	 */
	public Position getNextPosition() {
		return nextPosition;
	}
	
	/**
	 * Sets the next piece.
	 *
	 * @param nextPiece the new next piece
	 */
	public void setNextPiece(Piece nextPiece) {
		if(isActualPlayer(nextPiece.getPlayer())) this.nextPiece = nextPiece;
	}
	
	/**
	 * Sets the next position.
	 *
	 * @param nextPosition the new next position
	 */
	public void setNextPosition(Position nextPosition) {
		this.nextPosition = nextPosition;
	}
	
	/**
	 * This method actualize the state of the player, who play, if the game is finish, tell the IA to play.
	 * Its the "engine" of this game.
	 */
	public void nextTour(){
		
		Player actuel = getActualPlayer();
		PlayerZone playerZone = null;
		for(PlayerZone p : blokus.getViewGame().getPlayerZones()){
			if(actuel == p.getPlayer()){
				playerZone = p;
				playerZone.synchro();
			}
		}
		
		if(actuel != null && actuel.getCanPlay()){
			if(actuel.getActualNbPiece() == 0){
				actuel.cantPlay();
				playerZone.isFinish();
			} else if(!gr.getMap().isNextRound( actuel)){
				playerZone.isFinish();
				actuel.cantPlay();
			} else {
				playerZone.isWaiting();
			}
		}
		
		if(gr.playersCanPlay()){
			do{
				if(gr.getActualPlayer() >= this.gr.getPlayers().length - 1){
					gr.setActualPlayer(0);
				} else {
					gr.setActualPlayer((gr.getActualPlayer()) + 1);
				}
				actuel = gr.getPlayers()[gr.getActualPlayer()];
				if(actuel.getCanPlay()){
					if(!gr.getMap().isNextRound(actuel)){
						for(PlayerZone p : blokus.getViewGame().getPlayerZones()){
							if(actuel == p.getPlayer()){
								playerZone = p;
								p.isFinish();
							}
						}
						
						actuel.cantPlay();
					} else {
						for(PlayerZone p : blokus.getViewGame().getPlayerZones()){
							if(actuel == p.getPlayer()){
								playerZone = p;
								p.isPlaying();
							}
						}
					}
				}
			}while(!actuel.getCanPlay() && gr.playersCanPlay());
			if(actuel instanceof IA && actuel.getCanPlay()){
				IA ia = (IA)(actuel);
				ia.nextMove(gr);
				this.nextPiece = ia.getNextPiece();
				this.nextPosition = ia.getNextPosition();
				playerZone.synchro();
				this.confirmPosition();
			}
		}
		
		if(!gr.playersCanPlay()){
			blokus.getViewGame().getZoneInformation().afficherScore();
			gr.setActualPlayer(-1);
		}
	}
	
	/**
	 * Removes the next piece.
	 */
	public void removeNextPiece(){
		this.nextPiece = null;
	}

	/**
	 * If the nextPiece and the nextPosition are valid,this method place the piece in the
	 * map and the grid, and remove it in the player and return true.
	 * Else it return false.
	 * 
	 * @return true, if successful
	 */
	public boolean confirmPosition(){
		boolean ret = false;
		if(nextPosition != null && nextPiece != null && gr.getMap().addPiece(nextPiece, nextPosition)){
			ret = true;
			nextPiece.getPlayer().usePiece(nextPiece);
			blokus.getViewGame().getGrid().addPiece(nextPiece, nextPosition);
			this.removeNextPiece();
			blokus.getViewGame().getGrid().deleteLastTemporalPiece();
			
			if(lastPiece != null)lastPiece.actualiserBar();
			this.nextTour();
		}
		return ret;
	}

	/**
	 * Gets the map of this games gamerule.
	 *
	 * @return the map
	 */
	public Map getMap() {
		return gr.getMap();
	}
	
	/**
	 * Gets the gameRule of this game.
	 *
	 * @return the game rule
	 */
	public GameRule getGameRule(){
		return gr;
	}
	
	/**
	 * Checks if the player in param is the actual player.
	 *
	 * @param p the player to test
	 * @return true, if p is the actual player
	 */
	public boolean isActualPlayer(Player p){
		boolean ret = false;
		if(p == getActualPlayer()){
			ret = true;
		} else if(this instanceof GameOnline && ((PlayerOnline)p).getId() == gr.getActualPlayer()){
			ret = true;
		}
		return ret;
	}
}

package controler;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import view.LinePlayer;

/**
 * The Class ListenerPlayersMenu.
 */
public class ListenerPlayersMenu implements ActionListener{
	
	/** The action. */
	private int action;
	
	/** The color.. */
	private Color[] couleurs;
	
	/** The line. */
	private LinePlayer line;

	/**
	 * Instantiates a new listener players menu.
	 *
	 * @param action the action
	 */
	public ListenerPlayersMenu (int action) {
		this.action = action;
		Color[] c = {new Color(211, 47, 47), new Color(47, 211, 47), new Color(47, 47, 211), new Color(91, 60, 183), new Color(0, 145, 130), new Color(194, 24, 91), new Color(2, 136, 209)};
		this.couleurs = c;
	}
	
	/**
	 * Instantiates a new listener players menu.
	 *
	 * @param action the action
	 * @param line it represent a player
	 */
	public ListenerPlayersMenu (int action, LinePlayer line) {
		this.action = action;
		Color[] c = {new Color(211, 47, 47), new Color(47, 211, 47), new Color(47, 47, 211), new Color(91, 60, 183), new Color(0, 145, 130), new Color(194, 24, 91), new Color(2, 136, 209)};
		this.couleurs = c;
		this.line = line;
	}
	
	/**
	 * Manages the check button and color button.
	 *
	 * @param e the action event
	 */
	public void actionPerformed(ActionEvent e) {
		if (action == 1) {
			Color actual = ((JButton)(e.getSource())).getBackground();
			
			int i = 0;
			int j = 0;
			while(couleurs.length > i && j < 2){
				if (j > 0) {
					((JButton)(e.getSource())).setBackground(couleurs[i]);
					j++;
				}
				if (actual.equals(couleurs[i])) {
					j++;
					if (i == (couleurs.length-1)) {
						i = -1;
					}
				}
				i++;
			}
			if (j < 2) {
				((JButton)(e.getSource())).setBackground(couleurs[0]);
			}
		}
		else if (action == 2) {
			Color check = new Color(165, 110, 45);
			Color unCheck = new Color(35, 35, 35);

			if (((JButton)(e.getSource())).getBackground().equals(unCheck)) {
				((JButton)(e.getSource())).setBackground(check);
				if (line != null) {
					line.setLevelVisible(true);
				}
			
			} else{
				((JButton)(e.getSource())).setBackground(unCheck);
				if (line != null) {
					line.setLevelVisible(false);
				}
			}
		}
	}

	/**
	 * Gets the line who represent a player.
	 *
	 * @return the line
	 */
	public LinePlayer getLine() {
		return line;
	}

	/**
	 * Sets the line who represent a player.
	 *
	 * @param line the new line
	 */
	public void setLine(LinePlayer line) {
		this.line = line;
	}
	
	/**
	 * Gets the color.
	 *
	 * @param i the index of the color.
	 * @return the color
	 */
	public Color getColor(int i){
		return couleurs[i];
	}
}
package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.Player;
import view.PlayerZone;

/**
 * This listener is used in playerZone in the button abandonner and passer.
 */
public class PlayerButtonListener implements ActionListener{
	
	/** The player. */
	private Player player;
	
	/** The playeZone. */
	private PlayerZone playerZone;
	
	/** The game. */
	private Game game;
	
	/**
	 * Instantiates a new player button listener.
	 *
	 * @param p the player
	 * @param pz the playerZone
	 * @param g the game
	 */
	public PlayerButtonListener(Player p, PlayerZone pz, Game g){
		player = p;
		game = g;
		playerZone = pz;
	}
	
	/**
	 * When the user click on a button, if it's abandonner the player lose and the interface are adapted.
	 * Else if it's the button passer and if it's the round of the player, it's round is skip.
	 */
	public void actionPerformed(ActionEvent e){
		if(player.getCanPlay()){
			if(e.getSource() == playerZone.getBouttonPasser()){
				if(player == game.getActualPlayer()){
					game.removeNextPiece();
					game.nextTour();
				}
			} else if (e.getSource() == playerZone.getBouttonAbandonner()){
	            if(JOptionPane.showConfirmDialog (null, "Voulez-vous vraiment arreter ?", "Abandon", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					player.cantPlay();
					playerZone.isFinish();
					if(player == game.getActualPlayer()){
						game.removeNextPiece();
						game.nextTour();
					}
	            }
			}
		}
	}
}

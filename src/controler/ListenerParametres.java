package controler;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import view.Parametres;

/**
 * The Class ListenerParametres.
 */
public class ListenerParametres implements MouseListener{
	
	/** The parametres. */
	private Parametres parametres;
	
	/** The number piece. */
	private int numberPiece;
	
	/** The panel. */
	private JPanel panel;

	/**
	 * Instantiates a new listener parametres.
	 *
	 * @param parametres the parametres
	 * @param n the number piece
	 * @param p the panel
	 */
	public ListenerParametres(Parametres parametres, int n, JPanel p) {
		this.numberPiece = n;
		this.parametres = parametres;
		this.panel = p;
	}
	
	
	public void mousePressed(MouseEvent e) {
	}
	
	public void mouseReleased(MouseEvent e) {
	}
	
	public void mouseEntered(MouseEvent e) {
	}
	
	public void mouseExited(MouseEvent e) {
	}
	
	/**
	 * Add a piece for the next game if it's a left click, otherwise unselect it.
	 *
	 * @param parametres the parametres
	 * @param n the number piece
	 * @param p the panel
	 */
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			parametres.removePiece(numberPiece);
			panel.setBorder(new LineBorder(new Color(54, 54, 54), 2));
		}
		else{
			parametres.addPiece(numberPiece);
			panel.setBorder(new LineBorder(new Color(47, 175, 47), 2));
		}
	}
}

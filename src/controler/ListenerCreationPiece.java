package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import model.Pattern;
import model.Position;
import view.MenuPiece;

/**
 * The Class ListenerCreationPiece.
 */
public class ListenerCreationPiece implements MouseListener, ActionListener{
	
	/** The grid. */
	private MenuPiece grid;
	
	/**
	 * Instantiates a new listener creation piece.
	 *
	 * @param grid the grid
	 */
	public ListenerCreationPiece(MenuPiece grid) {
		this.grid = grid;
	}
	
	/**
	 * Instantiates a new listener creation piece.
	 *
	 * @param e the ActionEvent
	 */
	public void actionPerformed(ActionEvent e){
		Pattern p = new Pattern();
		if (p.addPattern(grid.getPiece())) {
			grid.flush();
			grid.refresh();
		}
		else{
			JOptionPane.showMessageDialog(null, "La piece existe deja.", "Ajout de piece", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * Add a block to the piece if it's a left click, otherwise remove it.
	 *
	 * @param e the MouseEvent
	 */
	public void mousePressed(MouseEvent e) {
		grid.add(getPosition((JLabel)e.getSource()));
		if (SwingUtilities.isRightMouseButton(e)) {
			grid.remove(getPosition((JLabel)e.getSource()));
		}
		else{
			grid.add(getPosition((JLabel)e.getSource()));
		}
	}
	
	public void mouseReleased(MouseEvent e) {
	}
	
	public void mouseEntered(MouseEvent e) {
		grid.addHover(getPosition((JLabel)e.getSource()));
	}
	
	public void mouseExited(MouseEvent e) {
		grid.removeHover(getPosition((JLabel)e.getSource()));
	}
	
	/**
	 * Add a block to the piece if it's a left click, otherwise remove it.
	 *
	 * @param e the MouseEvent
	 */
	public void mouseClicked(MouseEvent e) {
		grid.add(getPosition((JLabel)e.getSource()));
		if (SwingUtilities.isRightMouseButton(e)) {
			grid.remove(getPosition((JLabel)e.getSource()));
		}
		else{
			grid.add(getPosition((JLabel)e.getSource()));
		}
	}
	
	/**
	 * Gets the position.
	 *
	 * @param label the label
	 * @return the position of the label
	 */
	public Position getPosition(JLabel label){
		Position ret = null;
		JLabel[][] jLabelTab = grid.getjLabelTab();
		int y = 0;
		int x = 0;
		while(y < jLabelTab.length && ret == null){
			x = 0;
			while(x < jLabelTab[y].length && ret == null){
				if( (JLabel)jLabelTab[y][x] == label){
					ret = new Position(x,y);
				}
				x++;
			}
			y++;
		}
		return ret;
	}
}

package controler;

import java.awt.event.ActionEvent;

import model.PlayerOnline;
import view.Blokus;
import view.LinePlayer;

/**
 * The Class ListenerStartOnline.
 */
public class ListenerStartOnline extends ListenerStart{
	
	/** The iteration. */
	private int iteration;
	
	/** The players. */
	private PlayerOnline[] players;
	
	/**
	 * Instantiates a new listener start online.
	 *
	 * @param player1 the player 1
	 * @param player2 the player 2
	 * @param player3 the player 3
	 * @param player4 the player 4
	 * @param blokus the blokus
	 */
	public ListenerStartOnline(LinePlayer player1, LinePlayer player2, LinePlayer player3, LinePlayer player4, Blokus blokus) {
		super(player1, player2, player3, player4, blokus);
		iteration = 0;
	}
	
	/**
	 * Start and initialize the game for LAN.
	 *
	 * @param e the action event
	 */
	public void actionPerformed(ActionEvent e) {
		players = new PlayerOnline[2];
		players[0] = getPlayerObject(lines[0]);
		players[1] = getPlayerObject(lines[1]);
		
		new InitialisationOnlineGame(blokus, players);
	}
	
	/**
	 * Gets the playerOnline object from a playerLine.
	 *
	 * @param p a playerLine
	 * @return the playerOnline object
	 */
	protected PlayerOnline getPlayerObject(LinePlayer p){
		PlayerOnline ret = null;
		if (p != null) {
				ret = new PlayerOnline(p.getCouleur().getBackground(), p.getNom().getText(), 21, 42, iteration);
		}
		iteration++;
		return ret;
	}
}

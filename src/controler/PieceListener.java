package controler;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import view.Blokus;
import view.GlassPane;
import view.PlayerPiece;

/**
 * This listener action the drag and drop on the playerPiece
 */
public class PieceListener extends MouseAdapter implements MouseListener {
	
	/** Actif is if this listener is strated or stoped. */
	private boolean actif;
	
	/** The glassPane of the blokus. */
	private GlassPane glass;
	
	/** The ghost of the piece. (a image used to have a effect of drag) */
	private BufferedImage fantomePiece;
	
	/** The game. */
	private Game game;
	
	/**
	 * Instantiates a new piece listener.
	 *
	 * @param blokus the blokus
	 * @param game the game
	 */
	public PieceListener(Blokus blokus, Game game) {
		actif = true;
		this.glass = blokus.getGlass();
		this.game = game;
	}

	/**
	 * Stop this listener.
	 */
	public void stop() {
		actif = false;
	}

	/**
	 * Start this listener.
	 */
	public void start() {
		actif = true;
	}
	
	/**
	 * Execute the system of drag effect.
	 */
	public void mousePressed(MouseEvent e) {
		if (actif && game.isActualPlayer(((PlayerPiece)(e.getSource())).getPiece().getPlayer())){
			game.removeNextPiece();
			
			// Affiche la piece dans le glassPane
			Component maPiece = e.getComponent();
			Point location = (Point) e.getPoint().clone();
			SwingUtilities.convertPointToScreen(location, maPiece);
			SwingUtilities.convertPointFromScreen(location, glass);
			fantomePiece = new BufferedImage(maPiece.getWidth(), maPiece.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = fantomePiece.getGraphics();
			maPiece.paint(g);
			glass.setLocation(location);
			glass.setImage(fantomePiece);
			glass.setVisible(true);
		}
	}
	
	/**
	 * Execute the system of drag if the piece is not used.
	 */
	public void mouseReleased(MouseEvent e) {
		if (actif && game.isActualPlayer(((PlayerPiece)(e.getSource())).getPiece().getPlayer())){
			JComponent lab = (JComponent) e.getSource();
			TransferHandler handle = lab.getTransferHandler();
			// Transfert la piece
			handle.exportAsDrag(lab, e, TransferHandler.LINK);

			// Supprimer la piece temporaire du glasspane
			glass.setImage(null);
			glass.setVisible(false);
			
		}
	}
	
	/**
	 * This method update the position of this piece in the glassPane (when she is dragged).
	 */
	public void mouseDragged(MouseEvent e) {
		if (actif && game.isActualPlayer(((PlayerPiece)(e.getSource())).getPiece().getPlayer())){
			Component c = e.getComponent();
			Point p = (Point) e.getPoint().clone();
			SwingUtilities.convertPointToScreen(p, c);
			SwingUtilities.convertPointFromScreen(p, glass);
			glass.setLocation(p);
			glass.repaint();
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
	}
}

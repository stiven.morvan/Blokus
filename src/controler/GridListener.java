package controler;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import model.Position;
import view.Grid;

/**
 * The listener is used in the jpanel of the grid to know the position and if the player click.
 */
public class GridListener implements MouseListener, MouseWheelListener{
	
	/** The grid the game. */
	private Grid grid;
	
	/** The actual game. */
	private Game game;
	
	/**
	 * Instantiates a new grid listener.
	 *
	 * @param grid the grid of the game
	 * @param game the actual game
	 */
	public GridListener(Grid grid, Game game){
		this.grid = grid;
		this.game = game;
	}
	
	public void mousePressed(MouseEvent e) {
	}
	
	public void mouseReleased(MouseEvent e) {
	}
	
	/**
	 * Actualize the position of the cursor in the game (the attribute position of game)
	 * and add the nextPiece temporaly in the grid.
	 */
	public void mouseEntered(MouseEvent e) {
		Position p = this.getPosition((JLabel)e.getSource());
		game.setNextPosition(p);
		
		if(game.getNextPiece() != null){
			grid.addTemporalPiece(game.getNextPiece(), game.getNextPosition());
		}
	}
	
	/**
	 * Remove the last temporal piece.
	 */
	public void mouseExited(MouseEvent e) {
		grid.deleteLastTemporalPiece();
	}
	
	/**
	 * Reverse the piece if the player use the right click.
	 * Add the piece to the grid if the player use the left click.
	 */
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e) && game.getNextPiece() != null) {
			game.getNextPiece().reverse();
			grid.deleteLastTemporalPiece();
			grid.addTemporalPiece(game.getNextPiece(), game.getNextPosition());
		}
		else if(game.confirmPosition()){
				game.getLastPiece().actualiserBar();
				game.removeNextPiece();
				grid.deleteLastTemporalPiece();
		}
	}
	
	/**
	 * Rotate the piece of the grid if the sense of the wheel.
	 */
	public void mouseWheelMoved(MouseWheelEvent e) {
		try{
			int r = e.getWheelRotation();
	       if (r < 0) {
	    	   game.getNextPiece().rotateRight();
	       } else {
	    	   game.getNextPiece().rotateLeft();
	       }
	       grid.deleteLastTemporalPiece();
	       grid.addTemporalPiece(game.getNextPiece(), game.getNextPosition());
		}
		catch(NullPointerException ex){}
	}

	/**
	 * Gets the position of the jLabel on the grid.
	 *
	 * @param label the label
	 * @return the position of label
	 */
	public Position getPosition(JLabel label){
		Position ret = null;
		JLabel[][] jLabelTab = grid.getjLabelTab();
		int y = 0;
		int x = 0;
		while(y < jLabelTab.length && ret == null){
			x = 0;
			while(x < jLabelTab[y].length && ret == null){
				if( (JLabel)jLabelTab[y][x] == label){
					ret = new Position(x,y);

				}
				x++;
			}
			y++;
		}
		return ret;
	}
}
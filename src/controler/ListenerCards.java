package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import view.Blokus;
import view.MenuCreation;
import view.Parametres;

/**
 * The Class ListenerCards.
 */
public class ListenerCards implements ActionListener, MouseListener{
	
	/** The blokus frame. */
	private Blokus blokus;
	
	/** The name of the card who have this listener. */
	private String card;
	
	/** The path to the icon of this button. */
	private String icon;
	
	/**
	 * Instantiates a new listener cards.
	 *
	 * @param blokus the blokus
	 * @param card the card name
	 */
	public ListenerCards(Blokus blokus, String card) {
		this.blokus = blokus;
		this.card = card;
	}
	
	/**
	 * Instantiates a new listener cards with an icon.
	 *
	 * @param blokus the blokus
	 * @param card the card name
	 * @param icon the icon
	 */
	public ListenerCards(Blokus blokus, String card, String icon) {
		this(blokus, card);
		this.icon = icon;
	}
	
	
	/**
	 * Show the card define by attribute card
	 *
	 * @param e the ActionEvent
	 */
	public void actionPerformed(ActionEvent e){

		if(card.equals("Serveur")){
			Parametres param = null;
			MenuCreation menu = new MenuCreation(blokus, param, true);
			blokus.getCards().add(menu, "MenuOnline");
			blokus.show("MenuOnline");
		} else if (card.equals("Connexion")){
			String host = JOptionPane.showInputDialog(null, "Donnez l'identifiant du serveur : ", "Connexion a la partie", JOptionPane.QUESTION_MESSAGE);;
			if(host != null) new InitialisationOnlineGame(blokus, host);
		} else if (card.equals("Parametres")){
			blokus.show(this.card);
			blokus.getParametres().setPiece();
		} else {
			blokus.show(this.card);
		}
	}
	
	public void mousePressed(MouseEvent e) {
	}
	
	public void mouseReleased(MouseEvent e) {
	}
	
	public void mouseClicked(MouseEvent e) {
	}
	
	/**
	 * Change the icon when mouse hover
	 *
	 * @param e the ActionEvent
	 */
	public void mouseEntered(MouseEvent e) {
		JButton b = (JButton)e.getSource();
		b.setIcon(new ImageIcon("data/icons/"+icon+"Hover.png"));
	}
	
	/**
	 * Change the icon to normal
	 *
	 * @param e the ActionEvent
	 */
	public void mouseExited(MouseEvent e) {
		JButton b = (JButton)e.getSource();
		b.setIcon(new ImageIcon("data/icons/"+icon+".png"));
	}
}

package controler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import model.GameRuleOnline;
import model.PlayerOnline;
import model.ServeurBlokus;
import view.Blokus;
import view.ViewGame;

/**
 * The Class InitialisationOnlineGame, depending of the cunstructor used, will start a server and connect to him to play online.
 */
public class InitialisationOnlineGame {
	
	/** The host, the name of the computer who have the server. */
	private String host;
	
	/**
	 * Create a server who will start a gamerule with the players.
	 * After that, connect the program to this server.
	 * 
	 * This part of this program is experimental, the connection and the answer of the program can be defaulting.
	 *
	 * @param blokus the frame who display the game
	 * @param players the players of the futur game
	 */
	public InitialisationOnlineGame(Blokus blokus, PlayerOnline[] players) {
		new ServeurBlokus(players);
		try {
			this.host = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(null, "Pour vous connecter � la partie depuis un autre ordinateur, utilisez cet identifiant : " + host, "Connexion", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Attention : La partie s'affichera uniquement si l'autre joueur ce connecte.", "Connexion", JOptionPane.INFORMATION_MESSAGE);
		rechercheGame(blokus);
	}
	
	/**
	 * Connect the program to the computer of the name host.
	 * And display the game receive.
	 *
	 * @param blokus the blokus
	 * @param host the hostname (the name of the computer who have the server)
	 */
	public InitialisationOnlineGame(Blokus blokus, String host) {
		this.host = host;
		JOptionPane.showMessageDialog(null, "Si l'hote renseign� n'a pas lanc� le serveur ce programme restera static.", "Connexion", JOptionPane.INFORMATION_MESSAGE);
		rechercheGame(blokus);
	}
	
	/**
	 * Gets the name of the host.
	 *
	 * @return the host name
	 */
	public String getHost(){
		return host;
	}
	
	/**
	 * Search the server and get the gameRule.
	 * When the gameRule is find, this method create the GameOnline object and display the game.
	 * 
	 * This part of this program is experimental, the connection and the answer of the program can be defaulting.
	 *
	 * @param blokus the blokus
	 */
	public void rechercheGame(Blokus blokus){
		Socket socket;
		ObjectInputStream in;
		ObjectOutputStream out;
		GameRuleOnline gr = null;
		PlayerOnline player = null;
		boolean erreur = false;
		
		do {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			try {
				socket = new Socket(host, 2048);
				out = new ObjectOutputStream(socket.getOutputStream()); 
				out.writeObject(player);
				in = new ObjectInputStream(socket.getInputStream());
				if(player == null){
					player = (PlayerOnline) in.readObject();
				} else {
					gr = (GameRuleOnline) in.readObject();
				}
				socket.close();
				
			} catch (UnknownHostException e) {
			} catch (IOException e) {
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		} while (gr == null && !erreur);
		GameOnline game = new GameOnline(gr, blokus, player, host);
		blokus.setViewGame(new ViewGame(blokus, game));
		blokus.show("game");
		game.synchro();
	}
}

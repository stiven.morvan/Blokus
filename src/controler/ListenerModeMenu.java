package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * The Class ListenerModeMenu.
 */
public class ListenerModeMenu implements ActionListener{
	
	/** The main. */
	private JPanel main;
	
	/** The player 3. */
	private JPanel player3;
	
	/** The player 4. */
	private JPanel player4;
	
	/**
	 * This listener allow to switch between 2 and 4 player..
	 *
	 * @param main the main
	 * @param player3 the player 3
	 * @param player4 the player 4
	 */
	public ListenerModeMenu (JPanel main, JPanel player3, JPanel player4) {
		this.main = main;
		this.player3 = player3;
		this.player4 = player4;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (player4.isVisible() == true || player3.isVisible() == true) {
			((JButton)(e.getSource())).setText("Mode 4 Joueurs");
			player3.setVisible(false);
			player4.setVisible(false);
			main.setBorder(BorderFactory.createEmptyBorder(0,0,180,0));
		}
		else {
			((JButton)(e.getSource())).setText("Mode 2 Joueurs");
			player3.setVisible(true);
			player4.setVisible(true);
			main.setBorder(BorderFactory.createEmptyBorder(0,0,80,0));
		}
	} 
}

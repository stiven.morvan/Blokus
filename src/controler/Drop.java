package controler;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.TransferHandler;

import view.PlayerPiece;

/**
 * The Class Drop.
 */
public class Drop extends TransferHandler{
	
	/** The Constant PIECE_FLAVOR, this allow to have the same pointer between the object dragged and dropped */
	final public static DataFlavor PIECE_FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType+"; class=\"" + PlayerPiece.class.getCanonicalName()+"\"","MaPiece");
	
	/** The actual game. */
	private Game game;
	
	/**
	 * Instantiates a new drop object, who is used in the drag and drop of the pieces in the grid.
	 *
	 * @param g The actual Game.
	 */
	public Drop(Game g){
		this.game = g;
	}
	
	/**
	 * Return if the dropped object can be import.
	 * @param info the object droped
	 * @return A boolean, true if the object can be import (always)
	 */
	public boolean canImport(TransferHandler.TransferSupport info){
		return true;
	}
	
	/**
	 * Print the piece to the screen if its the player of this piece are the actualPlayer and use this piece (in the playerZone).
	 * @param support the object dropped
	 * @return A boolean, true if the object can be import (always)
	 */
	public boolean importData(TransferHandler.TransferSupport support){
		Transferable data = support.getTransferable();
		PlayerPiece piece;
		try {
			piece = (PlayerPiece)data.getTransferData(PIECE_FLAVOR);
			
			//Si la derniere piece n'est pas pos� ont la degrise.
			if(game.getLastPiece() != null){
				if(!game.getLastPiece().getPiece().getIsUse() && game.getLastPiece().getPiece().getPlayer().getCanPlay()) game.getLastPiece().reUse();
			}
			
			//On ajoute la prochaine piece a game
			if(game.getActualPlayer() != null){
				this.game.setNextPiece(piece.getPiece());
				this.game.setLastPiece(piece);
				//On grise la prochaine piece
				if(game.isActualPlayer(piece.getPiece().getPlayer())){
					piece.use();
					game.getGrid().addTemporalPiece(piece.getPiece(), game.getNextPosition());
				}
			} 
			
		} catch (UnsupportedFlavorException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
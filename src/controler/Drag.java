package controler;

import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import view.PlayerPiece;

/**
 * The Class Drag. This class is use in the system of drag and drop, to drag a piece in the grid.
 */
public class Drag extends TransferHandler{
	
	/**
	 * Return the playerPiece object corresponding to the playerPiece dragged.
	 * @param c the JComponent where the drag action is execute.
	 * @return The PlayerPiece object in the form of a transferable object.
	 */
	protected Transferable createTransferable(JComponent c) {
		return (PlayerPiece)c;
	}
	
	/**
	 * Return the type of action of the origin of this drag (a int).
	 * @param c the JComponent where the drag action is execute.
	 * @return A int corresponding to the action used.
	 */
	public int getSourceActions(JComponent c) {
		return LINK;
	}
}
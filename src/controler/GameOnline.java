package controler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import model.GameRuleOnline;
import model.PlayerOnline;
import view.Blokus;
import view.PlayerZone;

/**
 * The Class GameOnline is use only for the online gamemode.
 */
public class GameOnline extends Game implements Runnable {
	
	/** The player online. (the player of this computer) */
	private PlayerOnline playerOnline;
	
	/** The host. */
	private String host;

	/**
	 * Instantiates a new game online.
	 *
	 * @param gr the GameRule of this game
	 * @param blokus the frame who contains and display the game.
	 * @param player The player online. (the player of this computer) 
	 * @param host The host (the name of the server computer)
	 */
	public GameOnline(GameRuleOnline gr, Blokus blokus, PlayerOnline player, String host) {
		super(gr, blokus);
		this.host = host;
		playerOnline = player;
		Thread t = new Thread(this);
		t.start();
	}
	
	/**
	 * This thread actualize the gameRule between the server and this game. 
	 */
	public void run() {
		Socket socket;
		ObjectInputStream in;
		ObjectOutputStream out;
		GameRuleOnline gro = null;
		boolean fini = false;
		boolean end = false;
		while (gr.playersCanPlay() && !end) {
			try {
				Thread.sleep(500);
				socket = new Socket(host, 2048);
				out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject(playerOnline);
				in = new ObjectInputStream(socket.getInputStream());
				gro = (GameRuleOnline) in.readObject();
				socket.close();

				if (gro != null && gro.getVersion() > ((GameRuleOnline) gr).getVersion()) {
					// On atualise le GameRule
					this.gr = gro;
					((GameRuleOnline) gr).setLastPieceOnline(null);
					this.actualiserCanPlay();
					synchro();
				}
				
				this.actualiserCanPlay();
				fini = !playerOnline.getCanPlay();
				
				if (((GameRuleOnline) gr).getVersion() > gro.getVersion()) {
					if (gr.getActualPlayer() >= this.gr.getPlayers().length - 1) {
						gr.setActualPlayer(0);
					} else {
						gr.setActualPlayer((gr.getActualPlayer()) + 1);
					}
					
					socket = new Socket(host, 2048);
					out = new ObjectOutputStream(socket.getOutputStream());
					out.writeObject(playerOnline);
					out.writeObject((GameRuleOnline) gr);
					in = new ObjectInputStream(socket.getInputStream());
					gr = (GameRuleOnline) in.readObject();
					socket.close();
					synchro();
				} else if (fini && playerOnline.getId() == gro.getActualPlayer()) {
					if (gr.getActualPlayer() >= this.gr.getPlayers().length - 1) {
						gr.setActualPlayer(0);
					} else {
						gr.setActualPlayer((gr.getActualPlayer()) + 1);
					}
					((GameRuleOnline) gr).upVersion();

					socket = new Socket(host, 2048);
					out = new ObjectOutputStream(socket.getOutputStream());
					out.writeObject(playerOnline);
					out.writeObject((GameRuleOnline) gr);
					in = new ObjectInputStream(socket.getInputStream());
					gr = (GameRuleOnline) in.readObject();
					socket.close();
					synchro();
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				end = true;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		synchro();
		blokus.getViewGame().getZoneInformation().setGame(this);
		blokus.getViewGame().getZoneInformation().afficherScore();
	}
	
	/**
	 * This method actualize the playerOnline (if he have finish).
	 */
	private void actualiserCanPlay(){
		playerOnline = ((GameRuleOnline) gr).getPlayerById(playerOnline);
		
		if(playerOnline.getCanPlay()){
			if(playerOnline.getActualNbPiece() == 0){
				playerOnline.cantPlay();
			} else if(!gr.getMap().isNextRound(playerOnline)){
				playerOnline.cantPlay();
			}
		}
	}
	
	/**
	 * Synchronize this game (in view) with its GameRule. (used in the only mode and when we restart a game)
	 */
	public void synchro() {
		super.synchro();
		PlayerOnline player = null;

		for (PlayerZone p : blokus.getViewGame().getPlayerZones()) {
			p.hideButton();
			player = ((GameRuleOnline) gr).getPlayerById((PlayerOnline) p.getPlayer());
			if (player != null) {
				p.synchro(player);
				if (!player.getCanPlay()) {
					p.isFinish();
					p.desActiver();
				} else if (player.getId() == gr.getActualPlayer() && player.getId() == playerOnline.getId()) {
					p.isPlaying();
					p.activer();
				} else {
					p.isWaiting();
					p.desActiver();
				}
			}
		}
	}

	/**
	 * If the nextPiece and the nextPosition are valid,this method place the piece in the
	 * map and the grid, and remove it in the player and return true.
	 * Else it return false.
	 * 
	 * @return true, if successful
	 */
	public boolean confirmPosition() {
		boolean ret = false;
		if (nextPosition != null && nextPiece != null && gr.getMap().addPiece(nextPiece, nextPosition)) {
			ret = true;

			((GameRuleOnline) gr).setLastPieceOnline(nextPiece);

			nextPiece.getPlayer().usePiece(nextPiece);
			blokus.getViewGame().getGrid().addPiece(nextPiece, nextPosition);

			this.removeNextPiece();
			blokus.getViewGame().getGrid().deleteLastTemporalPiece();

			if (lastPiece != null)
				lastPiece.actualiserBar();
			((GameRuleOnline) gr).upVersion();
		}
		return ret;
	}
}

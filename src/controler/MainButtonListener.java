package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.GameRule;
import view.Blokus;
import view.ViewGame;

/**
 * This listener is used to listen the main button of a game (quitter and sauvegarder) and the save JtextField.
 */
public class MainButtonListener implements ActionListener{
	
	/** The blokus. */
	private Blokus blokus;
	
	/** The game. */
	private Game game;
	
	/**
	 * Instantiates a new mainButtonListener.
	 *
	 * @param blokus the blokus
	 */
	public MainButtonListener(Blokus blokus){
		this.blokus = blokus;
		this.game = null;
	}
	
	/**
	 * Instantiates a new mainButtonListener.
	 *
	 * @param blokus the blokus
	 * @param game the game
	 */
	public MainButtonListener(Blokus blokus, Game game){
		this(blokus);
		this.game = game;
	}
	
	/**
	 * Adapt a action depending on the source.s
	 */
	public void actionPerformed(ActionEvent e){
		if(blokus != null && blokus.getViewGame() != null){
			if(e.getSource() == blokus.getViewGame().getZoneInformation().getQuitter()){
				if(JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter ? (pensez a sauvegarder avant si vous voulez rejouer)", "Quitter la partie", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					blokus.show("Main");
					blokus.removeGame();
				}
			}else if(e.getSource() == blokus.getViewGame().getZoneInformation().getSauvegarder()){
				System.out.println("Sauvegarder");
				String nom = JOptionPane.showInputDialog(null, "Donnez un nom � cette partie :", "Sauvegarder la partie", JOptionPane.QUESTION_MESSAGE);;
				if(nom != null){
					if(game.getGameRule().testSave(nom)){
						game.getGameRule().save(nom);
						JOptionPane.showMessageDialog(null, "La sauvegarde est r�ussie.", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
					} else {
						if(JOptionPane.showConfirmDialog(null, "Une sauvegarde portant ce nom existe d�j�, voulez-vous vraiment sauvegarder avec ce nom ? (l'ancienne sauvegarder sera supprimer)", "Sauvegarder la partie", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
							game.getGameRule().save(nom);
							JOptionPane.showMessageDialog(null, "La sauvegarde est r�ussie.", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}
			}
		}
		if (e.getSource() instanceof JTextField) {
			//Verifie si la sauvegarde existe
			boolean saveExiste = false;
			int i = 0;
			String[] listeSave = (new File("data/save")).list();
			String nomSave = ((JTextField)e.getSource()).getText();
			while(!saveExiste && i < listeSave.length){
				if(listeSave[i].equals(nomSave + ".txt")){
					saveExiste = true;
				}
				i++;
			}
			
			
			if(saveExiste){
				GameRule gr = new GameRule(nomSave);
				game = new Game(gr, blokus);
				if(gr.playersCanPlay()){
					if(gr.getActualPlayer() == 0){
						gr.setActualPlayer(game.getGameRule().getPlayers().length - 1);
					} else {
						gr.setActualPlayer((gr.getActualPlayer()) - 1);
					}
				} else {
					gr.setActualPlayer(-1);
				}
				blokus.setViewGame(new ViewGame(blokus, game));
				game.synchro();
				game.nextTour();
				blokus.show("game");
			} else {
				JOptionPane.showMessageDialog(null, "La sauvegarde n'existe pas.", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}

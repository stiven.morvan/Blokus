package model;

import java.awt.Color;

/**
 * The Class PlayerOnline is used for the onlineGmae, it have the particularity to have a Id.
 */
public class PlayerOnline extends Player{
	
	/** The online id. */
	private int onlineId;
	
	/**
	 * Instantiates a new player online.
	 *
	 * @param color the color of the player
	 * @param name the name of the player
	 * @param min the minimal piece in the pattern
	 * @param max the maximal piece in the pattern
	 * @param id the id of the player
	 */
	public PlayerOnline(Color color, String name, int min, int max, int id){
		super(color, name);
		iniPieces(min,max);
		onlineId = id;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId(){
		return onlineId;
	}
}

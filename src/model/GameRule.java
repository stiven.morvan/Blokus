package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * The Class GameRule.
 */
public class GameRule implements Serializable{
	
	/** The array of players. */
	protected Player[] players;
	
	/** The pattern. */
	private Pattern pattern;
	
	/** The map. */
	private Map map;
	
	/** Index of the actual player. */
	private int actualPlayer;
	
	/** Skip lap boolean. */
	private boolean passerTour;
	
	/**
	 * Instantiates a new game rule who specify all the rule and parameter of the game.
	 *
	 * @param players all the player of the game
	 */
	public GameRule(Player[] players){
		this.pattern = new Pattern();
		map = new Map();
		passerTour = true;
		
		int u = 0;
		while(u < players.length && players[u] != null) u++;
		this.players = new Player[u];
		actualPlayer = this.players.length - 1;
		u--;
		while(u > -1){
			this.players[u] = players[u];
			this.players[u].setPattern(this.pattern);
			u--;
		}
		
		this.setRandom();
	}
	
	/**
	 * Instantiates a new game rule.
	 *
	 * @param playersView the players view
	 * @param passerTour the passer tour
	 */
	public GameRule(Player[] playersView, boolean passerTour){
		this(playersView);
		this.passerTour = passerTour;
	}
	
	/**
	 * Instantiates a new game rule.
	 *
	 * @param name the name of the save
	 */
	public GameRule(String name){
		try{
			ObjectInputStream input = new ObjectInputStream(new FileInputStream("data/save/"+name+".txt"));
			GameRule gr = (GameRule)input.readObject();
			this.players = gr.players;
			this.pattern = gr.pattern;
			this.map = gr.map;
			this.actualPlayer = gr.actualPlayer;
			input.close();
    	} catch (ClassNotFoundException e) {
    		e.printStackTrace();
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Gets the value of skip lap.
	 *
	 * @return the skip lap
	 */
	public boolean getPasserTour() {
		return passerTour;
	}

	/**
	 * Sets a random order for the players and there relative origin corner of start.
	 */
	public void setRandom(){
		if (players != null && players.length > 0) {
			int i = 0;
			int u = 0;
			Player temp;
			do{
				temp = this.players[i];
				u =	(int)(Math.random() * ((players.length - i) - 1));
				players[i] = players[u];
				players[u] = temp;
				
				i++;
			}while( i < players.length - 1);
			
			players[0].setOrigin(new Position(0, 0));
			if (players.length >= 2) {
				players[1].setOrigin(new Position(19, 19));
				if (players.length >= 3) {
					players[1].setOrigin(new Position(19, 0));
					players[2].setOrigin(new Position(0, 19));
					if (players.length == 4) {
						players[3].setOrigin(new Position(19, 19));
						players[3].iniOrigin();
					}
					players[2].iniOrigin();
				}
				players[1].iniOrigin();
			}
			players[0].iniOrigin();
		}
	}
	
	/**
	 * Gets the score tab.
	 *
	 * @return the score tab
	 */
	public Player[] getScoreTab(){
		Player[] ret = new Player[players.length];
		
		for (int i = 0; i < ret.length; i++) {
			ret[i] = players[i];
		}
		
		Player temp = null;
		int max;
		int playerMax = 0;
		
		for (int i = 0; i < ret.length; i++) {
			temp = ret[i];
			max = -1;
			playerMax = i;
			for (int j = i; j < ret.length; j++) {
				if(ret[j].getScore() >= max){
					max = ret[j].getScore();
					playerMax = j;
				}
			}
			ret[i] = ret[playerMax];
			ret[playerMax] = temp;
		}
		
		return ret;
	}
	
	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public Player[] getPlayers() {
		return players;
	}
	
	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public Map getMap(){
		return map;
	}
	
	/**
	 * Gets the actual player.
	 *
	 * @return the actual player
	 */
	public int getActualPlayer(){
		return actualPlayer;
	}
	
	/**
	 * Sets the actual player.
	 *
	 * @param a the new actual player
	 */
	public void setActualPlayer(int a){
		this.actualPlayer = a;
	}
	
	/**
	 * check if a player can play.
	 *
	 * @return true, if successful
	 */
	public boolean playersCanPlay(){
		boolean ret = false;
		int i = 0;
		while(i < players.length && !ret){
			if(players[i].getCanPlay()) ret = true;
			i++;
		}
		return ret;
	}
	
	/**
	 * Test if the save can be done.
	 *
	 * @param nom the name of the save
	 * @return true, if successful
	 */
	public boolean testSave(String nom){
		boolean canSave = true;
		int i = 0;
		String[] listefichiers = (new File("data/save")).list();
		
		while(canSave && i < listefichiers.length){
			if(listefichiers[i].equals(nom + ".txt")){
				canSave = false;
			}
			i++;
		}
		return canSave;
	}
	
	/**
	 * Save the game with the specify name.
	 *
	 * @param nom the name of the save
	 */
	public void save(String nom){
		try {
			ObjectOutputStream output;
			output = new ObjectOutputStream(new FileOutputStream("data/save/"+nom+".txt"));
			output.writeObject(this);
			output.close();
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
	}
}

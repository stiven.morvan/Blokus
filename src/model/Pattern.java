package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The Class Pattern.
 * Get pattern from pattern file and generate for each pattern his fullPattern.
 */
public class Pattern implements Serializable {
	
	/** The pattern path. */
	final public String PATTERN_PATH = "data/pattern/pattern.txt";
	
	/** The pattern position path. */
	final public String PATTERN_POSITION_PATH = "data/pattern/patternPosition.txt";
	
	/** The pattern number. */
	static public int PATTERN_NUMBER;
	
	/** The pattern cache. */
	private boolean[][][] patternCache;
	
	/** The pattern position cache. */
	private int[][][] patternPositionCache;
	
	/**
	 * Instantiates a new pattern class who manage all the pattern of the application.
	 */
	public Pattern(){
		int i = 0;
		int i2 = 0;
		
		try {
			FileWriter writer = new FileWriter(PATTERN_POSITION_PATH, true);
			writer.close();
	    } catch (FileNotFoundException fnfe) {
	        System.out.println(fnfe);
	    }
	    catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		i = getNumberOfLine(PATTERN_PATH);
		i2 = getNumberOfLine(PATTERN_POSITION_PATH);
	    
	    this.PATTERN_NUMBER = i;
	    this.patternCache = new boolean[PATTERN_NUMBER][5][5];
	    for(int j = 0; j < PATTERN_NUMBER; j++){
	    	this.patternCache[j] = null;
	    }
	    this.patternPositionCache = new int[PATTERN_NUMBER][7][7];
	    for(int j = 0; j < PATTERN_NUMBER; j++){
	    	this.patternPositionCache[j] = null;
	    }
	    
	    generatePatternPosition(i, i2);
	    
	}
	
	/**
	 * Generate pattern position.
	 *
	 * @param patternIndex the pattern index
	 * @param patternPositionIndex the pattern position index
	 */
	private void generatePatternPosition(int patternIndex, int patternPositionIndex){
		if (patternIndex > patternPositionIndex) {
			FileWriter f;
			PrintWriter p;
		    try {
		    	f = new FileWriter(PATTERN_POSITION_PATH, true);
		        p = new PrintWriter(f);
		        for (int i = patternPositionIndex; i < (patternIndex); i++) {
		        	getPatternAt(i);
		        	patternPositionCache[i] = computePatternPosition(patternCache[i]);
					p.println(patternToString(patternPositionCache[i]));
				}
		        p.close();
		    } catch (FileNotFoundException fnfe) {
		        System.out.println(fnfe);
		    }
		    catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		else if(patternIndex < patternPositionIndex){
			File f = new File(PATTERN_POSITION_PATH);
			try{
				PrintWriter writer = new PrintWriter(f);
				writer.print("");
				writer.close();
			}
			catch(Exception e){
				
			}
			generatePatternPosition(patternIndex, 0);
		}
	}
	
	/**
	 * Compute pattern position.
	 *
	 * @param originPattern the origin pattern
	 * @return the pattern for positioning empty if origin pattern was null.
	 */
	private int[][] computePatternPosition(boolean[][] originPattern){
		int[][] ret = new int[7][7];
		if (originPattern != null) {
			for (int y = 1; y < 6; y++) {
				for (int x = 1; x < 6; x++) {
					if (originPattern[x-1][y-1]) {
						ret[x+1][y] = 2;
						ret[x-1][y] = 2;
						ret[x][y+1] = 2;
						ret[x][y-1] = 2;
					}
				}
			}
			for (int y = 1; y < 6; y++) {
				for (int x = 1; x < 6; x++) {
					if (originPattern[x-1][y-1]) {
						if (ret[x-1][y+1] == 0) {
							ret[x-1][y+1] = 3;
						}
						if (ret[x-1][y-1] == 0) {
							ret[x-1][y-1] = 3;
						}
						if (ret[x+1][y+1] == 0) {
							ret[x+1][y+1] = 3;
						}
						if (ret[x+1][y-1] == 0) {
							ret[x+1][y-1] = 3;
						}
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * Gets the number of line of a file.
	 *
	 * @param path the path
	 * @return the number of line
	 */
	private int getNumberOfLine(String path){
		int ret =0;
		
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(path));
		    while (buffer.readLine() != null) {
		    	ret++;
			}
			buffer.close();
		}
	    catch(FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
	    }
	    catch (IOException e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * Read and generate the pattern at a specified index.
	 *
	 * @param i the index
	 * @return the pattern at the index i
	 */
	private void getPatternAt(int i){
		BufferedReader buffer;
		String line;
	    try {
			buffer = new BufferedReader(new FileReader(PATTERN_PATH));
			buffer.skip(i*27);
		    if ((line = buffer.readLine()) != null) {
		    	patternCache[i] = stringToPattern(line, 5);
			}
			buffer.close();
		}
	    catch(FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
	    }
	    catch (IOException e) 
		{
			e.printStackTrace();
		}
	    patternPositionCache[i] = computePatternPosition(patternCache[i]);
	}
	
	/**
	 * Gets the pattern at a specified index.
	 *
	 * @param i the index
	 * @return the pattern
	 */
	public boolean[][] getPattern(int i){
		boolean[][] ret = new boolean[5][5];
		if (patternCache[i] != null) {
			System.arraycopy( patternCache[i], 0, ret, 0, 5 );
		}
		else{
			getPatternAt(i);
			System.arraycopy( patternCache[i], 0, ret, 0, 5 );
		}
		
		return ret;
	}
	
	/**
	 * Gets the full pattern (piece + positioning pattern).
	 *
	 * @param i the index
	 * @return the full pattern
	 */
	public int[][] getFullPattern(int i){
		int[][] ret = new int[7][7];
		if (i < 0) {
			i = 0;
		}
		if (i > PATTERN_NUMBER-1) {
			i = 0;
		}
		if (patternPositionCache[i] != null && patternCache[i] != null) {
			System.arraycopy( patternPositionCache[i], 0, ret, 0, 7 );
		}
		else{
			getPatternAt(i);
			System.arraycopy( patternPositionCache[i], 0, ret, 0, 7 );
		}
		
		for (int y = 1; y < 6; y++) {
			for (int x = 1; x < 6; x++) {
				if (patternCache[i][x-1][y-1]) {
					ret[x][y] = patternCache[i][x-1][y-1] ? 1 : 0;
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * Convert a Pattern to a String.
	 *
	 * @param pattern the pattern
	 * @return the string
	 */
	private String patternToString(int[][] pattern){
		String ret = "";
		if(pattern != null && pattern[0] != null){
			int size = pattern[0].length;
			for (int y = 0; y < size; y++) {
				for (int x = 0; x < size; x++) {
					ret = ret + pattern[x][y];
				}
			}
		}
		return ret;
	}
	
	/**
	 * Convert a String to a Pattern.
	 *
	 * @param s the s
	 * @param size the size
	 * @return the boolean[][]
	 */
	private boolean[][] stringToPattern(String s, int size){
		boolean[][] ret = new boolean[size][size];
		if(s.length() == (size*size)){
			int i = 0;
			for (int y = 0; y < size; y++) {
				for (int x = 0; x < size; x++) {
					if(s.charAt(i) == '1'){
						ret[x][y] = true;
					}
					i++;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Checks if there is already a piece like that in the pattern file.
	 *
	 * @param s the string representing the piece
	 * @return true, if there is this piece
	 */
	public boolean isPiece(String s){
		boolean ret = true;
		String line;
		for (int i = 0; i < 25; i++) {
			if (s.charAt(i) == '1') {
				ret = false;
			}
		}
		if (!ret) {
			try {
				BufferedReader buffer = new BufferedReader(new FileReader(PATTERN_PATH));
			    while ((line = buffer.readLine()) != null && !ret) {
			    	for (int i = 0; i < 25; i++) {
						if (s.equals(line)) {
							ret = true;
						}
					}
				}
				buffer.close();
			}
		    catch(FileNotFoundException exc) {
				System.out.println("Erreur d'ouverture");
		    }
		    catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	/**
	 * Gets the all collection of piece.
	 *
	 * @return the all collection
	 */
	public ArrayList<Piece> getAllCollection(){

	    ArrayList<Piece> ret = new ArrayList<Piece>();
		for (int i = 0; i < PATTERN_NUMBER; i++) {
			ret.add(new Piece(i, this));
		}
		
		return ret;
	}
	
	/**
	 * Adds a pattern.
	 *
	 * @param tab the pattern to add
	 * @return true, if successful
	 */
	public boolean addPattern(int[][] tab){
		FileWriter f;
		PrintWriter p;
		boolean ret = false;
		if (!isPiece(patternToString(tab))) {
			if (tab != null) {
				try {
			    	f = new FileWriter(PATTERN_PATH, true);
			        p = new PrintWriter(f);
			        p.println(patternToString(tab));
			        p.close();
			        ret = true;
			    } catch (FileNotFoundException fnfe) {
			        System.out.println(fnfe);
			    }
			    catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		
		return ret;
	}
}

package model;

import java.io.Serializable;

/**
 * The Class Piece represent the different piece possible with the blokus.
 */
public class Piece implements Serializable{
	
	/** The pattern of the piece. */
	private int[][] pattern;
	
	/** The player. */
	private Player player;
	
	/** If this piece is use or not. */
	private boolean isUse;
	
	/** The score of the piece. */
	private int score;
	
	/**
	 * Instantiates a new piece.
	 *
	 * @param numPattern the number of the piece in the pattern
	 * @param pattern the pattern
	 * @param player the player
	 */
	public Piece(int numPattern, Pattern pattern, Player player){	
		this.pattern = pattern.getFullPattern(numPattern);
		isUse = false;
		this.player = player;
		this.setScore();
	}
	
	/**
	 * Instantiates a new piece.
	 *
	 * @param numPattern the number of the piece in the pattern
	 * @param pattern the pattern
	 */
	public Piece(int numPattern, Pattern pattern){	
		this.pattern = pattern.getFullPattern(numPattern);
		isUse = false;
		this.setScore();
	}
	
	/**
	 * Set the score.
	 */
	public void setScore(){
		int score = 0;
		for(int[] i : pattern){
			for(int u : i){
				if(u == 1) score++;
			}
		}
		this.score = score;
	}
	
	/**
	 * Get the score of the piece.
	 *
	 * @return the score
	 */
	public int getScore(){
		return this.score;
	}
	
	/**
	 * Get the player of the piece.
	 *
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Set the player of the piece.
	 *
	 * @param player the new player
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Use this piece.
	 */
	public void use(){
		isUse = true;
	}
	
	/**
	 * Checks if this piece is use.
	 *
	 * @return the state of the piece
	 */
	public boolean getIsUse(){
		return isUse;
	}

	/**
	 * Gets the pattern.
	 *
	 * @return the pattern
	 */
	public int[][] getPattern() {
		return pattern;
	}
	
	/**
	 * Rotate the piece on the right.
	 */
	public void rotateRight(){
		int[][] rotate = new int[7][7];

	    for (int i = 0; i < 7; ++i) {
	        for (int j = 0; j < 7; ++j) {
	            rotate[i][j] = pattern[7 - j - 1][i];
	        }
	    }
	    
	    pattern = rotate;
	}
	
	/**
	 * Rotate the piece on the left.
	 */
	public void rotateLeft(){
		int[][] rotate = new int[7][7];

	    for (int i = 0; i < 7; ++i) {
	        for (int j = 0; j < 7; ++j) {
	            rotate[i][j] = pattern[j][7 - i - 1];
	        }
	    }
	    
	    pattern = rotate;
	}
	
	/**
	 * Reverse this piece.
	 */
	public void reverse(){
		int[][] reverse = new int[7][7];
		int j = 6;
	    for (int i = 0; i < 7; ++i) {
	    	for (int y = 0; y < 7; ++y) {
	        	reverse[j][y] = pattern[i][y];
	        }
	        j--;
	    }
	    
	    pattern = reverse;
	}
	
	/**
	 * This method return the smallest representation of the pattern of this piece.
	 * @return The smallest pattern (a board of boolean)
	 */
	public boolean[][] getMinimumPattern(){
		boolean[][] minPattern = new boolean[1][1];
		
		int maxX = 0;
		int maxY = 0;
		int minX = 6;
		int minY = 6;
		
		//on determine la taille minimal( les lignes ou les variables sont a 1 )
		for (int y = 0; y < pattern.length; y++) {
			for (int x = 0; x < pattern[y].length; x++) {
				if(pattern[y][x] == 1){
					if(y < minY) minY = y;
					if(y > maxY) maxY = y;
					if(x < minX) minX = x;
					if(x > maxX) maxX = x;
				}
			}
		}
		try{
			minPattern = new boolean[(maxY - minY) + 1][(maxX - minX) + 1];
			for (int i = minY; i <= maxY; i++) {
				for (int u = minX; u <= maxX; u++) {
					if( pattern[i][u] == 1) minPattern[i - minY][u - minX] = true;
					else minPattern[i - minY][u - minX] = false;
				}
			}
		}
		catch(Exception e){
		}
		
		return minPattern;
	}
}

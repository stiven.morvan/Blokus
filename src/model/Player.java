package model;

import java.awt.Color;
import java.io.Serializable;

/**
 * The Class Player represent and store the characteristic of a player.
 */
public class Player implements Serializable{
	
	/** The pattern number. */
	final public int PATTERN_NUMBER;
	
	/** The board of the players piece. */
	private Piece[] pieces;
	
	/** The color of the players. */
	private Color color;
	
	/** The score of the players. */
	private int score;
	
	/** The name of the players. */
	private String name;
	
	/** The pattern of the players. */
	private Pattern pattern;

	/** The map of the players. */
	private boolean[][] map;
	
	/** If the player can play. */
	private boolean canPlay;
	
	/** The origin of the players. */
	private Position origin;
	
	/** The actual number of piece. */
	private int actualNbPiece;
	
	/**
	 * Instantiates a new player.
	 *
	 * @param color the color of the players
	 * @param name the name of the players
	 */
	public Player(Color color, String name){
		this.color = color;
		this.name = name;
		score = 0;
		map = new boolean[20][20];
		canPlay = true;
		pattern = new Pattern();
		
		PATTERN_NUMBER = 21;
		actualNbPiece = 21;
		iniPieces(0, 21);
	}
	
	/**
	 * Instantiates a new player.
	 *
	 * @param color the color of the players
	 * @param name the name of the players
	 * @param pattern the pattern of the players
	 */
	public Player(Color color, String name, Pattern pattern){
		this(color, name);
		this.pattern = pattern;
	}
	
	/**
	 * Instantiates a new player.
	 *
	 * @param color the color of the players
	 * @param name the name of the players
	 * @param pattern the pattern of the players
	 * @param pieces the pieces of the players
	 */
	public Player(Color color, String name, Pattern pattern, int[] pieces){
		this.color = color;
		this.name = name;
		score = 0;
		map = new boolean[20][20];
		canPlay = true;
		this.pattern = pattern;
		iniPieces(pieces);
		PATTERN_NUMBER = actualNbPiece;
	}
	
	/**
	 * Return the pattern number.
	 * 
	 * @return the pattern number (a int)
	 */
	public int getPATTERN_NUMBER() {
		return PATTERN_NUMBER;
	}
	
	/**
	 * Set if the player can play.
	 * 
	 * @param canPlay true if he can play
	 */
	public void setCanPlay(boolean canPlay) {
		this.canPlay = canPlay;
	}

	/**
	 * Get the origin of the players.
	 *
	 * @return the origin of the players
	 */
	public Position getOrigin() {
		return origin;
	}

	/**
	 * Set the origin of the players.
	 *
	 * @param origin the new origin
	 */
	public void setOrigin(Position origin) {
		this.origin = origin;
	}
	
	/**
	 * Initialise the origin of the players.
	 */
	public void iniOrigin(){
		map[origin.getX()][origin.getY()] = true;
	}
	
	/**
	 * Set the canPlay attribute to false.
	 */
	public void cantPlay(){
		this.canPlay = false;
	}
	
	/**
	 * Get the canPlay attribute.
	 *
	 * @return the canPlay attribute
	 */
	public boolean getCanPlay(){
		return canPlay;
	}

	/**
	 * Initialize the piece of the player.
	 *
	 * @param start the starting piece number
	 * @param end the ending piece number
	 */
	protected void iniPieces(int start, int end){
		if(start > end){
			int temp = start;
			start = end;
			end = temp;
		}
		if (start < 0) {
			start = 0;
		}
		if ((end - start) > 21){
			end = start+21;
		}
		pieces = new Piece[(end-start)];
		for (int i = start; i < end; i++) {
			pieces[i-start] = new Piece(i, this.pattern, this);
		}
	}
	
	/**
	 * Initialize the piece of the player.
	 *
	 * @param tab the board of piece
	 */
	private void iniPieces(int[] tab){
		int size = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] != -1) {
				size++;
			}
		}
		pieces = new Piece[size];
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] != -1) {
				pieces[actualNbPiece] = new Piece(tab[i], this.pattern, this);
				actualNbPiece++;
			}
		}
	}

	/**
	 * Get the actual number piece.
	 *
	 * @return the actual number piece
	 */
	public int getActualNbPiece(){
		return actualNbPiece;
	}
	
	/**
	 * Use a piece of the player.
	 *
	 * @param p the piece to use
	 */
	public void usePiece(Piece p){
		int i = 0;
		while(i < pieces.length){
			if(pieces[i] == p){
				pieces[i].use();
				score += pieces[i].getScore();
				actualNbPiece--;
				if(pieces[i].getScore() == 1 && getActualNbPiece() == 0) score += 5;
				i = pieces.length;
			}
			i++;
			
			if(getActualNbPiece() == 0) score += 15;
		}
		
	}
	
	/**
	 * Get the pieces of the player.
	 *
	 * @return the pieces of the player
	 */
	public Piece[] getPieces() {
		return pieces;
	}

	/**
	 * Get the color.
	 *
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the color.
	 *
	 * @param color the new color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Get the score.
	 *
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Set the score.
	 *
	 * @param score the new score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Get the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	

	/**
	 * Get the pattern.
	 *
	 * @return the pattern
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * Get the map.
	 *
	 * @return the map
	 */
	public boolean[][] getMap() {
		return map;
	}

	/**
	 * Set the map.
	 *
	 * @param map the new map
	 */
	public void setMap(boolean[][] map) {
		this.map = map;
	}

	/**
	 * Set the pattern.
	 *
	 * @param pattern the new pattern
	 */
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}
}

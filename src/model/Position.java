package model;

import java.io.Serializable;

/**
 * The Class Position is explicit, this class is a landmark used in a lot of board.
 */
public class Position implements Serializable{
	
	/** The x. */
	private int x;
	
	/** The y. */
	private int y;
	
	/**
	 * Instantiates a new position.
	 */
	public Position(){
		x = 0;
		y = 0;
	}
	
	/**
	 * Instantiates a new position.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Position(int x, int y){
		this.x = x;
		this.y = y;
	}

	/**
	 * Get the x.
	 *
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Set the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Get the y.
	 *
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Set the y.
	 *
	 * @param y the new y
	 */
	public void setY(int y) {
		this.y = y;
	}
}

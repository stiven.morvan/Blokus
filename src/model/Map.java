package model;

import java.io.Serializable;

/**
 * The Class Map.
 */
public class Map implements Serializable{
	
	/** The map. */
	private Piece[][] map;
	
	/**
	 * Instantiates a new map.
	 */
	public Map(){
		map = new Piece[20][20];
	}
	
	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public Piece[][] getMap() {
		return map;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the new map
	 */
	public void setMap(Piece[][] map) {
		this.map = map;
	}

	/**
	 * Check if the specify piece and position can be place on the map.
	 *
	 * @param piece the piece
	 * @param position the position
	 * @return -1 if something block the piece, 1 if the piece can be place and 0 otherwise.
	 */
	public int tryAdd(Piece piece, Position position){
		int ret = 0;
		int x = -3;
		int y = -3;
		int posX = position.getX();
		int posY = position.getY();
		int absoluteX = 0;
		int absoluteY = 0;
		Boolean samePlayer;
		if (!piece.getIsUse()) {
			while (y<4 &&  ret != -1) {
				absoluteY = posY + y;
				while (x<4 && ret != -1) {
					absoluteX = posX + x;
					
					if (absoluteX >= 0 && absoluteX < 20 && absoluteY >= 0 && absoluteY < 20) {
						try {
							if(map[absoluteY][absoluteX] != null){
								samePlayer = (map[absoluteY][absoluteX].getPlayer() == piece.getPlayer()) || (piece.getPlayer() instanceof PlayerOnline && ((PlayerOnline)map[absoluteY][absoluteX].getPlayer()).getId() == ((PlayerOnline)piece.getPlayer()).getId());
								if (piece.getPattern()[y+3][x+3] == 1) {
									ret = -1;
								}
								if (piece.getPattern()[y+3][x+3] == 3 && samePlayer) {
									ret = 1;
								}
								else if (piece.getPattern()[y+3][x+3] == 2 && samePlayer) {
									ret = -1;
								}
							}
						} catch (IndexOutOfBoundsException e) {
						}
					}
					else if (absoluteX == -1 && absoluteY == -1 && piece.getPlayer().getOrigin().getX() == 0 && piece.getPlayer().getOrigin().getY() == 0 && piece.getPattern()[y+3][x+3] == 3) {
						ret = 1;
					}
					else if (absoluteX == 20 && absoluteY == -1 && piece.getPlayer().getOrigin().getX() == 0 && piece.getPlayer().getOrigin().getY() == 19 && piece.getPattern()[y+3][x+3] == 3) {
						ret = 1;
					}
					else if (absoluteX == -1 && absoluteY == 20 && piece.getPlayer().getOrigin().getX() == 19 && piece.getPlayer().getOrigin().getY() == 0 && piece.getPattern()[y+3][x+3] == 3) {
						ret = 1;
					}
					else if (absoluteX == 20 && absoluteY == 20 && piece.getPlayer().getOrigin().getX() == 19 && piece.getPlayer().getOrigin().getY() == 19 && piece.getPattern()[y+3][x+3] == 3) {
						ret = 1;
					}
					else if (piece.getPattern()[y+3][x+3] == 1) {
						ret = -1;
					}
					x++;
				}
				x = -3;
				y++;
			}
		}
		return ret;
	}
	
	/**
	 * Adds the piece to the map and check it with tryAdd.
	 *
	 * @param piece the piece
	 * @param position the position
	 * @return true, if successful
	 */
	public boolean addPiece(Piece piece, Position position){
		boolean ret = false;
		if (tryAdd(piece, position) == 1) {
			int pX = position.getX();
			int pY = position.getY();
			
			boolean[][] mapPlayer = piece.getPlayer().getMap();
			for (int x = -3; x < 4; x++) {
		    	for (int y = -3; y < 4; y++) {
					if(piece.getPattern()[y+3][x+3] == 1){
						map[y+pY][x+pX] = piece;
					}
					if ((pX + x) >= 0 && (pX + x) < 20 && (pY + y) >= 0 && (pY + y) < 20) {
						if (mapPlayer[y+pY][x+pX]) {
							if(piece.getPattern()[y+3][x+3] == 1 || piece.getPattern()[y+3][x+3] == 2){
								mapPlayer[y+pY][x+pX] = false;
							}
						}
						if(piece.getPattern()[y+3][x+3] == 3 && map[y+pY][x+pX] == null){
							mapPlayer[y+pY][x+pX] = true;
						}
					}
				}
			}
			ret = true;
		}
		return ret;
	}
	
	/**
	 * Checks if the next round will be possible to play.
	 *
	 * @param p the player
	 * @return true, if there is a next round.
	 */
	public boolean isNextRound(Player p){
		boolean ret = false;
		int y = 0;
		int x = 0;
		if (p.getActualNbPiece() != 0) {
			while (!ret && x<20) {
				while (!ret && y<20) {
					if (p.getMap()[y][x]) {
						for (int i = 0; i < p.PATTERN_NUMBER; i++) {
							if (!p.getPieces()[i].getIsUse() && !ret) {
								if (tryAll(p.getPieces()[i], new Position(x, y)) != 10) {
									ret = true;
								}
							}
						}
					}
					y++;
				}
				x++;
				y = 0;
			}
		}
		if (p.getActualNbPiece() == p.PATTERN_NUMBER) {
			ret = true;
		}
		return ret;
	}
	
	/**
	 * Try all usable piece in all different ways but do not change the initial state of the piece.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @return number of rotation when the piece was possible to place, 10 otherwise..
	 */
	public int tryAll(Piece p, Position pos){
		int ret = 0;
		int j = 0;
		boolean b = false;
		if (p != null && pos != null) {
			while (!b && j<2) {
				for (int x = -3; x < 4; x++) {
					for (int y = -3; y < 4; y++) {
						if (p.getPattern()[x+3][y+3] == 1 && !b) {
							ret = 0;
							for (int i = 0; i < 4; i++) {
								if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1 && !b) {
									b = true;
								}
								p.rotateRight();
								if (!b) {
									ret++;
									if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1) {
										b = true;
									}
								}
								
							}
						}
					}
				}
				if (!b) {
					p.reverse();
					j++;
				}
			}
		}
		if (!b) {
			ret = 10;
		}
		return ret;
	}
	
	/**
	 * Gets the next piece to place for an IA.
	 *
	 * @param p the player (IA)
	 * @param level the level
	 * @return the next piece
	 */
	public Object[] getNextPiece(Player p, int level, GameRule gr){
		Object[] ret = new Object[2];
		int x=0, y=0;
		Object[] weight = new Object[2];
		int w = -100;
		boolean first = false;
		int rand = 0;
		if (p.getActualNbPiece() == p.getPattern().PATTERN_NUMBER) {
			first = true;
		}
		
		if (p.getActualNbPiece() != 0) {
			while (x<20) {
				while (y<20) {
					if (p.getMap()[y][x]) {
						for (int i = 0; i < p.PATTERN_NUMBER; i++) {
							if (!p.getPieces()[i].getIsUse()) {
								if (first) {
									rand = (int)(Math.random()*p.PATTERN_NUMBER);
									weight = tryAllWeight(p.getPieces()[rand], new Position(x, y), level, w, gr);
									if (weight != null && weight[0] != null) {
										ret[0] = p.getPieces()[rand];
										ret[1] = weight[0];
									}
								}
								else{
									weight = tryAllWeight(p.getPieces()[i], new Position(x, y), level, w, gr);
									if (weight != null && weight[0] != null && (int)weight[1] > w) {
										w = (int)weight[1];
										ret[0] = p.getPieces()[i];
										ret[1] = weight[0];
									}
								}
								
							}
						}
					}
					y++;
				}
				x++;
				y = 0;
			}
		}
		return ret;
	}
	
	/**
	 * Try all usable piece in all different ways and change the initial state of the piece.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @return the position when the piece was possible to place, null otherwise..
	 */
	public Position tryAllKeepRotate(Piece p, Position pos){
		Position ret = null;
		int j = 0;
		boolean b = false;
		if (p != null && pos != null) {
			while (!b && j<2) {
				for (int x = -3; x < 4; x++) {
					for (int y = -3; y < 4; y++) {
						if (p.getPattern()[x+3][y+3] == 1 && !b) {
							for (int i = 0; i < 4; i++) {
								if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1 && !b) {
									b = true;
									ret = new Position(x+pos.getX(), y+pos.getY());
								}
								if (!b) {
									p.rotateRight();
									if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1) {
										b = true;
										ret = new Position(x+pos.getX(), y+pos.getY());
									}
								}
							}
						}
					}
				}
				if (!b) {
					p.reverse();
					j++;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Try all usable piece in all different ways and change the initial state of the piece.
	 *
	 * @param p the player
	 * @param pos the position
	 * @param level the level
	 * @param minWeight the min weight
	 * @param gr the GameRule
	 * @return the object[], the weight and position of the piece
	 */
	public Object[] tryAllWeight(Piece p, Position pos, int level, int minWeight, GameRule gr){
		int rotate = 0;
		int[] tab = new int[3];
		tab[0] = minWeight;
		Object[] ret = new Object[2];
		ret[0] = null;
		int j = 0;
		int w = 100;
		boolean b = false;
		
		if (p != null && pos != null) {
			while (!b && j<2) {
				for (int x = -3; x < 4; x++) {
					for (int y = -3; y < 4; y++) {
						if (p.getPattern()[x+3][y+3] == 1 && !b) {
							rotate = 0;
							for (int i = 0; i < 4; i++) {
								if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1) {
									w = getWeight(p, new Position(x+pos.getX(), y+pos.getY()), level, gr);
									if (w > tab[0]) {
										tab[0] = w;
										tab[1] = rotate;
										tab[2] = j;
										ret[1] = w;
										ret[0] = new Position(x+pos.getX(), y+pos.getY());
									}
								}
								p.rotateRight();
								rotate++;
								if (tryAdd(p, new Position(x+pos.getX(), y+pos.getY())) == 1) {
									w = getWeight(p, new Position(x+pos.getX(), y+pos.getY()), level, gr);
									if (w > tab[0]) {
										tab[0] = w;
										tab[1] = rotate;
										tab[2] = j;
										ret[1] = w;
										ret[0] = new Position(x+pos.getX(), y+pos.getY());
									}
								}
							}
						}
					}
				}
				p.reverse();
				j++;
			}
		}
		if (w == 100) {
			ret = null;
		}
		else{
			if (tab[2] == 1) {
				p.reverse();
			}
			for (int i = 0; i < tab[1]; i++) {
				p.rotateRight();
			}
		}
		return ret;
	}
	
	/**
	 * Gets the weight.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @param level the level
	 * @param gr the GameRule
	 * @return the weight
	 */
	public int getWeight(Piece p, Position pos, int level, GameRule gr){
		int ret = 0;
		int adj = getAdjacent(p, pos);
		if (level == 1) {
			ret += adj;
			ret -= (p.getScore())*2;
		}
		else if (level == 2) {
			ret += adj;
			ret -= getAdjacentActualPlayer(p, pos);
			ret += (p.getScore()*2)-2;
			ret -= (p.getScore()-1);
		}
		else if (level >= 3) {
			ret += (p.getScore()-1)*6;
			ret += (getAncre(p, pos))*2;
			ret -= adj;
			ret -= getAdjacentActualPlayer(p, pos);
			ret += (Math.abs(pos.getX() - p.getPlayer().getOrigin().getX()) + Math.abs(pos.getY() - p.getPlayer().getOrigin().getY()))/5;
			ret += getAncreOtherPlayer(p, pos, gr)*2.5;
		}
		return ret;
	}
	
	/**
	 * Gets the adjacent.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @return the adjacent
	 */
	public int getAdjacent(Piece p, Position pos){
		int pX = pos.getX();
		int pY = pos.getY();
		int ret = 0;
		for (int x = -3; x < 4; x++) {
			for (int y = -3; y < 4; y++) {
				if ((pX + x) >= 0 && (pX + x) < 20 && (pY + y) >= 0 && (pY + y) < 20) {
					if (p.getPattern()[x+3][y+3] == 2 && map[x+pX][y+pY] != null && map[x+pX][y+pY].getPlayer() != p.getPlayer()) {
						ret++;
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * Gets the anchor.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @return the ancre
	 */
	public int getAncre(Piece p, Position pos){
		int pX = pos.getX();
		int pY = pos.getY();
		int ret = 0;
		for (int x = -3; x < 4; x++) {
			for (int y = -3; y < 4; y++) {
				if ((pX + x) >= 0 && (pX + x) < 20 && (pY + y) >= 0 && (pY + y) < 20) {
					if (p.getPattern()[x+3][y+3] == 3 && map[x+pX][y+pY] == null) {
						ret++;
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * Gets the anchor of other player.
	 *
	 * @param p the player
	 * @param pos the pos
	 * @return the ancre
	 */
	public int getAncreOtherPlayer(Piece p, Position pos, GameRule gr){
		int pX = pos.getX();
		int pY = pos.getY();
		boolean[][] mapPlayers = new boolean[20][20];
		for (int i = 0; i < gr.getPlayers().length-1; i++) {
			for (int x = 0; x < mapPlayers.length; x++) {
				for (int y = 0; y < mapPlayers.length; y++) {
					mapPlayers[x][y] = gr.getPlayers()[i].getMap()[x][y]; 
				}
			}
		}
		int ret = 0;
		for (int x = -3; x < 4; x++) {
			for (int y = -3; y < 4; y++) {
				if ((pX + x) >= 0 && (pX + x) < 20 && (pY + y) >= 0 && (pY + y) < 20) {
					if (p.getPattern()[x+3][y+3] == 1 && mapPlayers[x+pX][y+pY]) {
						ret++;
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * Gets the adjacent actual player.
	 *
	 * @param p the p
	 * @param pos the pos
	 * @return the adjacent actual player
	 */
	public int getAdjacentActualPlayer(Piece p, Position pos){
		int pX = pos.getX();
		int pY = pos.getY();
		int ret = 0;
		for (int x = -3; x < 4; x++) {
			for (int y = -3; y < 4; y++) {
				if ((pX + x) >= 0 && (pX + x) < 20 && (pY + y) >= 0 && (pY + y) < 20) {
					if ((p.getPattern()[x+3][y+3] == 0 || p.getPattern()[x+3][y+3] == 3) && map[x+pX][y+pY] != null && map[x+pX][y+pY].getPlayer() == p.getPlayer()) {
						ret++;
					}
				}
			}
		}
		return ret;
	}
}
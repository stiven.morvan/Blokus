package model;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * This class start the main socket and launch the serverPlayer with the players.
 */
public class ServeurBlokus {

	/**
	 * Instantiates a new serverBlokus.
	 *
	 * @param players the players
	 */
	public ServeurBlokus(PlayerOnline[] players) {

		ServerSocket socketServeur;
		try {
			socketServeur = new ServerSocket(2048);
			Thread t = new Thread(new ServeurPlayer(socketServeur, players));
			t.start();
		} catch (IOException e) {
		}
	}

}

package model;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class is the manager of the gameRule on a online game.
 */
public class ServeurPlayer implements Runnable {

	/** The main socket of the server. */
	private ServerSocket socketServeur;
	
	/** The socket use to transfer the gameRule. */
	private Socket socket;
	
	/** The board of attribution of player for this game. */
	private PlayerOnline[] attribution;
	
	/** The GameRule. */
	private GameRuleOnline gr;

	/**
	 * Instantiates a new server player and run it.
	 *
	 * @param socketServeur the socketServeur
	 * @param players the players
	 */
	public ServeurPlayer(ServerSocket socketServeur, PlayerOnline[] players) {
		this.socketServeur = socketServeur;
		gr = new GameRuleOnline(players);
		gr.setActualPlayer(0);
		
		attribution = new PlayerOnline[gr.getPlayers().length];
		for (int i = 0; i < attribution.length; i++) {
			attribution[i] = (PlayerOnline) gr.getPlayers()[i];
		}
	}
	
	/**
	 * This method send the gameRule and update it according to the player message.
	 */
	public void run() {
		ObjectInputStream in;
		PlayerOnline temp = null;
		ObjectOutputStream out;
		GameRuleOnline grTemp = null;
		boolean fini = false;
		
		while (gr.playersCanPlay()) {
			try {
				socket = socketServeur.accept();
				in = new ObjectInputStream(socket.getInputStream());
				temp = (PlayerOnline) in.readObject();
				if (temp == null) {
					temp = attribut();
					out = new ObjectOutputStream(socket.getOutputStream());
					out.writeObject(temp);
					out.flush();
				} else if (attibutionFini()) {
					// La partie a commenc�e
					out = new ObjectOutputStream(socket.getOutputStream());
					out.writeObject(gr);
					out.flush();
					grTemp = (GameRuleOnline) in.readObject();
					if (grTemp != null && grTemp.getVersion() > gr.getVersion()) {
						gr = grTemp;
					}
				}
				socket.close();
			} catch (IOException | ClassNotFoundException e) {
			}
		}
		
		//Permet de donner une deniere fois le GameRule
		while(!fini){
			try {
				fini = true;
				socket = socketServeur.accept();
				in = new ObjectInputStream(socket.getInputStream());
				temp = (PlayerOnline) in.readObject();
				
				for (int i = 0; i < attribution.length; i++) {
					attribution[i] = (PlayerOnline) gr.getPlayers()[i];
				}
				for (int i = 0; i < attribution.length; i++) {
					if(attribution[i] != null){
						fini = false;
						if(temp != null && attribution[i].getId() == temp.getId()) attribution[i] = null;
					}
				}
				
				out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject(gr);
				out.flush();
				socket.close();
			} catch (IOException | ClassNotFoundException e) {
			}
		}
		try {
			socketServeur.close();
		} catch (IOException e) {
		}
	}

	/**
	 * Check if the Attibution is ended.
	 *
	 * @return true, if it's finish
	 */
	public boolean attibutionFini() {
		boolean ret = true;
		int i = 0;
		while (ret && i < attribution.length) {
			if (attribution[i] != null) {
				ret = false;
			}
			i++;
		}
		return ret;
	}

	/**
	 * Attribute a player to a user.
	 *
	 * @return a the playerOnline
	 */
	public PlayerOnline attribut() {
		PlayerOnline ret = null;
		int i = 0;
		while (ret == null && i < attribution.length) {
			if (attribution[i] != null) {

				ret = attribution[i];
				attribution[i] = null;
			}
			i++;
		}
		return ret;
	}
}
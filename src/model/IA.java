package model;

import java.awt.Color;

/**
 * The Class IA.
 */
public class IA extends Player{
	
	/** The level. */
	private int level;
	
	/** The next piece. */
	private Piece nextPiece;
	
	/** The next position. */
	private Position nextPosition;
	
	/** The game rule. */
	private GameRule gameRule;

	/**
	 * Instantiates a new ia.
	 *
	 * @param color the color of the ia.
	 * @param name the of the ia
	 */
	public IA(Color color, String name){
		super(color, name);
		level = 1;
	}
	
	/**
	 * Instantiates a new ia.
	 *
	 * @param color the color of the ia.
	 * @param name the of the ia.
	 * @param level the level of the ia.
	 */
	public IA(Color color, String name, int level){
		super(color, name);
		this.level = level;
	}
	
	/**
	 * Instantiates a new AI.
	 *
	 * @param color the color of the AI.
	 * @param name the of the AI.
	 * @param pattern the pattern of the game
	 * @param pieces the all the pieces
	 * @param level the level of the AI.
	 */
	public IA(Color color, String name, Pattern pattern, int[] pieces, int level){
		super(color, name, pattern, pieces);
		this.level = level;
	}
	
	/**
	 * Compute the next move of this AI.
	 *
	 * @param gr the gameRule.
	 */
	public void nextMove(GameRule gr){
		Map m = gr.getMap();
		if (m != null) {
			Object[] o = m.getNextPiece(this, level, gr);
			nextPiece = (Piece)o[0];
			nextPosition = (Position)o[1];
		}
	}

	/**
	 * Gets the game rule.
	 *
	 * @return the game rule
	 */
	public GameRule getGameRule() {
		return gameRule;
	}

	/**
	 * Sets the game rule.
	 *
	 * @param gameRule the new game rule
	 */
	public void setGameRule(GameRule gameRule) {
		this.gameRule = gameRule;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * Gets the next piece.
	 *
	 * @return the next piece
	 */
	public Piece getNextPiece() {
		return nextPiece;
	}

	/**
	 * Sets the next piece.
	 *
	 * @param nextPiece the new next piece
	 */
	public void setNextPiece(Piece nextPiece) {
		this.nextPiece = nextPiece;
	}

	/**
	 * Gets the next position.
	 *
	 * @return the next position
	 */
	public Position getNextPosition() {
		return  nextPosition;
	}

	/**
	 * Sets the next position.
	 *
	 * @param nextPosition the new next position
	 */
	public void setNextPosition(Position nextPosition) {
		this.nextPosition = nextPosition;
	}
}

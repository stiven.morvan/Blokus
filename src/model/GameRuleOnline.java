package model;

// TODO: Auto-generated Javadoc
/**
 * The Class GameRuleOnline.
 */
public class GameRuleOnline extends GameRule{

	/** The version. */
	private int version;
	
	/** The last piece online. */
	private Piece lastPieceOnline;
	
	/**
	 * Instantiates a new game rule who specify all the rule and parameter of the game.
	 *
	 * @param players all the player of the game
	 */
	public GameRuleOnline(PlayerOnline[] playersView) {
		super(playersView);
		version = 0;
	}
	
	/**
	 * Sets the last piece place on the game.
	 *
	 * @param lastPieceOnline the last piece placed
	 */
	public void setLastPieceOnline(Piece lastPieceOnline) {
		this.lastPieceOnline = lastPieceOnline;
	}

	/**
	 * Gets the player by id.
	 *
	 * @param p the player
	 * @return the player by id
	 */
	public PlayerOnline getPlayerById(PlayerOnline p){
		PlayerOnline ret = null;
		int u = 0;
		int i = p.getId();
		while(u < players.length && ret == null){
			if(((PlayerOnline) players[u]).getId() == i){
				ret = (PlayerOnline) players[u];
			}
			u++;
		}
		return ret;
	}
	
	/**
	 * Up version.
	 */
	public void upVersion(){
		version++;
	}

	/**
	 * Gets the last piece place on the game.
	 *
	 * @return the last piece place on the game.
	 */
	public Piece getLastPieceOnline() {
		return lastPieceOnline;
	}
	
	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public int getVersion(){
		return version;
	}
}
